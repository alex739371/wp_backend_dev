<?php
/**
 * @package MC Bing Map
 * @version 1.6
 */
/*
Plugin Name: MC Bing Map
Plugin URI: 
Author: 
Description: Bing Map
Version: 1.0
*/

define( 'MC_PLUGIN_URL', plugin_dir_url(__FILE__) );

function wp_enqueue_bing_map() {
	wp_enqueue_style( 'bm_css', MC_PLUGIN_URL . 'css/style.css' );
	//wp_enqueue_script( 'bm_js', MC_PLUGIN_URL . 'js/script.js' );
	//$data = MC_PLUGIN_URL . 'images/pin.png';
	//wp_localize_script( "bm_js", "data", $data );
}
add_action( 'wp_footer', 'wp_enqueue_bing_map' );

function randomkeys($length){
	$pattern='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ,./<>?;#:@~[]{}-_=+)(*&^%___FCKpd___0pound;"!'; //字符池
	for($i=0;$i<$length;$i++){
		$key.=$pattern{mt_rand(0,35)};
	}
	return $key;
}

function bm_function( $atts, $content = null ) {
	extract( shortcode_atts( array(		
		'c' => '',
		'h' => ''
	), $atts ) );

	wp_reset_query();
	global $post;
	$location = get_post_meta( $post->ID, 'location', true );
	$l_l = get_post_meta( $post->ID, 'l_l', true );
    $cor = $c ? $c : $l_l;
    $h = $h ? $h : 310;

    $id = randomkeys(8);
	$html = '<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&mkt=zh-HK"></script>';
	$html .= '<div id="mc_map_' . $id . '" class="mc_map" style="height:' . $h . 'px"></div>';
	$html .= '<script type="text/javascript">';
		$html .= 'jQuery(document).ready(function($) {';
			//$html .= 'window.onload = function() {';
				//$html .= "if($('#mc_map').length) {";
					$html .= 'var map = new Microsoft.Maps.Map(document.getElementById("mc_map_' . $id . '"),{credentials: "AtGOa4X6oRYX2QForzxMYd3bqUySeyu-eA2IDpe1qTVdqCUsuKEdlqBR7jmodIqR",center: new Microsoft.Maps.Location(' . $cor . '),mapTypeId: Microsoft.Maps.MapTypeId.road,zoom: 15});';
					$html .= 'var center = map.getCenter();';
					$html .= 'var pin = new Microsoft.Maps.Pushpin(center, {icon: "' . MC_PLUGIN_URL . 'images/pin.png", width: 20, height: 33, draggable: false});';
					$html .= 'map.entities.push(pin);';
				//$html .= '}';				
			//$html .= '}';
		$html .= '});';
	$html .= '</script>';

	return $html;
}
add_shortcode( 'bm', 'bm_function' );

function bm_list_function( $atts, $content = null ) {
	extract( shortcode_atts( array(		
	), $atts ) );

	wp_reset_query();
	global $post;
	$html = '';
	$a_sidebar = get_post_meta( $post->ID, 'a_sidebar', true );
	if( $a_sidebar > 0 ) {
		for ($i=0; $i < $a_sidebar; $i++) { 
			$title = get_post_meta( $post->ID, 'a_sidebar_' . $i . '_title', true );
			$desc = get_post_meta( $post->ID, 'a_sidebar_' . $i . '_desc', true );
			$html .= '<div class="widget widget_archive sidebar_box">';
			if( $title ) {
				$html .= '<h3 class="box_header">' . $title . '</h3><!-- box_header -->';
			}
			if( $desc ) {
				$html .= '<div class="textwidget">' . do_shortcode( $desc ) . '</div>';
			}			
			$html .= '</div>';		
		}	
	}	
	return $html;
}
add_shortcode( 'bm_list', 'bm_list_function' );