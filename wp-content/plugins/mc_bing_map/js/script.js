jQuery(document).ready(function($) {
	$(window).load(function() {
		if($('#mc_map').length) {
			var c = $('.mc').attr('data-c');
			console.log(c);
	        var map = new Microsoft.Maps.Map(document.getElementById("mc_map"), 
	     		{
	     			credentials: "AtGOa4X6oRYX2QForzxMYd3bqUySeyu-eA2IDpe1qTVdqCUsuKEdlqBR7jmodIqR",
	     			center: new Microsoft.Maps.Location(c),
	     			mapTypeId: Microsoft.Maps.MapTypeId.road,
	     			zoom: 7
	     		});
	        var center = map.getCenter();
	        var pin = new Microsoft.Maps.Pushpin(center, {icon: data, width: 32, height: 52, draggable: true}); 
	        map.entities.push(pin);
		}
	});
});