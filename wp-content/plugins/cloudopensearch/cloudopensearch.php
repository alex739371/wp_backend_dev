<?php
/*
 Plugin Name: CloudOpenSearch
 Plugin URI: http://www.aliyun.com/product/opensearch/
 Description: 为wordpress提供一个专业的搜索功能的插件，用户可以点击左侧菜单中的开放搜索按钮，来自定义搜索相关的配置。
 Version: 0.1.1
 Author: Aliyun .inc
 Author URI: http://www.aliyun.com/product/opensearch/
 */

require_once 'opensearch-plugin.php';
require_once 'sdk/CloudsearchClient.php';
require_once 'sdk/CloudsearchSearch.php';

$opensearchPlugin = new OpensearchPlugin();