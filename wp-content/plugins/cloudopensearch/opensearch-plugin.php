<?php
class OpensearchPlugin {

    private $opensearchKeyId = NULL;
    private $opensearchKeySecret = NULL;
    private $opensearchHost = NULL;
    private $opensearchApp = NULL;
    private $opensearchHits = 20;
    private $opensearchDebug = false;

    private $client = NULL;

    private $defaultIndex = 'default';
    private $searchFormat = 'json';
    private $successful = false;

    public function __construct() {

        add_action('admin_menu', array($this, 'opensearchMenu'));
        add_action('admin_init', array($this, 'init'));

        if (!is_admin()) {

            add_action('pre_get_posts', array($this, 'search'));
            add_filter('the_posts', array( $this, 'getSearchResult'));
            $this->initializeApiClient();
        }
    }

    public function initializeApiClient() {
        $this->opensearchKeyId = get_option('opensearch_key_id');
        $this->opensearchKeySecret = get_option('opensearch_key_secret');
        $this->opensearchHost = get_option('opensearch_host');
        $this->opensearchApp = get_option('opensearch_app');
        $this->opensearchHits = get_option('opensearch_hits');
        $this->opensearchDebug = get_option('opensearch_debug') == 1 ? true : false;

        $this->client = new CloudsearchClient(
                $this->opensearchKeyId,
                $this->opensearchKeySecret,
                array('host' => $this->opensearchHost, 'debug' => $this->opensearchDebug),
                'aliyun'
        );
    }

    public function isSearch($wp_query) {
        if (function_exists('is_main_query') && ! $wp_query->is_main_query()) {
            return false;
        } elseif (is_search() && !is_admin()) {
            return true;
        } else {
            return false;
        }
    }

    public function search($wp_query) {

        if (!$this->isSearch($wp_query)) {
            return false;
        }

        $query = get_search_query(false);
        $page = get_query_var('paged');
        if (empty($page)) $page = 1;

        $start = ($page - 1) * $this->opensearchHits;
        $hits = $this->opensearchHits;

        $this->search = new CloudsearchSearch($this->client);
        $this->search->addIndex($this->opensearchApp);
        $this->search->setStartHit($start);
        $this->search->setHits($hits);
        $this->search->setQueryString($this->defaultIndex . ":'{$query}'");
        $this->search->setFormat($this->searchFormat);
        $result = $this->search->search();
        if ($this->opensearchDebug) {
            echo "<!--" . $this->client->getRequest() . "-->";
        }

        $result = json_decode($result, true);
        if ($result['status'] == 'OK') {
            foreach ($result['result']['items'] as $v) {
                $this->ids[] = $v['id'];
            }

            $this->maxPage = ceil($result['result']['viewtotal'] / $this->opensearchHits);
            $this->total = $result['result']['total'];

            $this->result = $result['result']['items'];
            $this->successful = true;
        } else {
            $this->successful = false;
        }
    }

    public function getSearchResult($posts) {
        if(!is_search() || !$this->successful) {
            return $posts;
        }

        global $wp_query;
        $wp_query->max_num_pages = $this->maxPage;
        $wp_query->found_posts = $this->total;

        $posts = array();
        foreach ($this->result as $v) {
            $v['ID'] = $v['id'];
            unset($v['id']);
            $v['post_date'] = date("Y-m-d H:i:s", substr($v['post_date'], 0, 10));
            $v['post_date_gmt'] = date("Y-m-d H:i:s", substr($v['post_date_gmt'], 0, 10));
            $v['post_modified'] = date("Y-m-d H:i:s", substr($v['post_modified'], 0, 10));
            $v['post_modified_gmt'] = date("Y-m-d H:i:s", substr($v['post_modified_gmt'], 0, 10));
            $posts[] = get_post((object) $v);
        }

        return $posts;
    }

    public function opensearchMenu() {
        add_menu_page('开放搜索', '开放搜索', 'manage_options', "opensearch", array( $this, 'opensearchAdminPage' ), plugins_url( 'assets/logo.png', __FILE__ ) );
    }

    public function opensearchAdminPage() {
        include('opensearch-admin.php' );
    }

    public function init() {
        if (current_user_can('manage_options')) {
            add_action('admin_menu', array($this, 'opensearchMenu'));
        }

        if (isset($_POST['action']) && $_POST['action'] == 'set_opensearch_config') {
            $hits = intval($_POST['opensearch_hits']);

            $hits = empty($hits) ? 20 : $hits;

            update_option('opensearch_key_id', trim($_POST['opensearch_key_id']));
            update_option('opensearch_key_secret', trim($_POST['opensearch_key_secret']));
            update_option('opensearch_host', trim($_POST['opensearch_host']));
            update_option('opensearch_app', trim($_POST['opensearch_app']));
            update_option('opensearch_hits',$hits);
            update_option('opensearch_debug', trim($_POST['opensearch_debug']));
        }
    }
}