<?php
$opensearchKeyId = get_option('opensearch_key_id');
$opensearchKeySecret = get_option('opensearch_key_secret');
$opensearchHost = get_option('opensearch_host');
$opensearchApp = get_option('opensearch_app');
$opensearchHits = get_option('opensearch_hits');
$opensearchDebug = get_option('opensearch_debug');

?>
<div class="wrap">
<style>
label {
  display: block;
}

li {
  padding-bottom: 7px
}
</style>
<h2 class="opensearch-header">阿里云开放搜索插件</h2><br/>
<form name="opensearch_settings" method="post" action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>">
		<input type="hidden" name="action" value="set_opensearch_config">
		<table class="widefat" style="width: 650px;">
			<thead>
				<tr>
					<th class="row-title">配置开放搜索插件必需的基本参数</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
					   开放搜索服务（Open Search）是解决用户结构化数据搜索需求的托管服务，支持数据结构、搜索排序、数据处理自由定制。 开放搜索服务为您的网站或应用程序提供简单、低成本、稳定、高效的搜索解决方案。<br><br>Access Key Id 和 Access Key Secret 可以通过登录阿里云后访问<br><a href="https://ak-console.aliyun.com/#/accesskey" target=_blank>https://ak-console.aliyun.com/#/accesskey</a> 获得。<br>host可以通过控制台查看应用的基本信息中的API入口。

						<ul>
							<li>
								<label>Access Key Id:</label>
								<input type="text" name="opensearch_key_id" required class="regular-text" value="<?php echo $opensearchKeyId; ?>" />
							</li>
							<li>
								<label>Access Key Secret:</label>
								<input type="password" name="opensearch_key_secret" required class="regular-text" value="<?php echo $opensearchKeySecret; ?>" />
							</li>
							<li>
								<label>Host:</label>
								<input type="text" name="opensearch_host" required class="regular-text" value="<?php echo $opensearchHost; ?>" />
							</li>
							<li>
								<label>应用名称:</label>
								<input type="text" name="opensearch_app" required class="regular-text" value="<?php echo $opensearchApp; ?>" />
							</li>
							<li>
						        <label>每页显示记录数:</label>
								<input type="text" style='width:40px' name="opensearch_hits" class="regular-text" value="<?php echo $opensearchHits; ?>" /><span style='color:#999'> 可选，默认为：20</span>
							</li>
							<li>
							    <label>打开调试模式:</label>
							    <input type=checkbox value=1 name=opensearch_debug <?php if ($opensearchDebug == 1) { echo "checked"; } ?>>
							    <span style='color:#999'> 指定调试模式后，会在页面的源码中打印开放搜索的请求连接。</span>
							 </li>
							<li>
								<label></label>
								<input type="submit" name="Submit" value="保存" class="button-primary" />
							</li>
					</td>
				</tr>
			</tbody>
		</table>
	</form>

</div>