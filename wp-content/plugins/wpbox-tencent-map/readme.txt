=== Plugin Name ===
Contributors:zzwrq
Donate link: http://www.wpbox.cn
Tags: map
Requires at least: 3.0.1
Tested up to: 3.8
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
WP盒子地图插件（腾讯地图基本版）是由WP盒子(www.wpbox.cn)出品的腾讯地图插件，启用后可直接在文章中插入腾讯地图，并有地图小工具。

使用方法：
1、在编辑模式选择插入图标直接插入，代为为：[wpboxmap latlng="39.916527,116.397128" address="中国,北京,故宫" width="500" height="300" zoom="3"][/wpboxmap]，参数释义：latlng：坐标值，address：中文地址，width：显示宽度，height:显示高度；
2、在wordpress后台的外观->小工具中，可添加WP盒子地图小工具，填写相应参数后保存即可。

相关链接：http://www.wpbox.cn/wlnmp/
== Installation ==
1. Upload this file to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
