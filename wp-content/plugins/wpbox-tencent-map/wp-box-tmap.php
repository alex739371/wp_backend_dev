<?php
/*
Plugin Name: WP盒子地图插件（腾讯地图基本版）
Plugin URI: http://www.wpbox.cn
Version: 1.0
Author:WP盒子(www.wpbox.cn)
Author URI: http://www.wpbox.cn
Description: WP盒子地图插件（腾讯地图基本版），启用后可直接在文章中插入腾讯地图，并有地图小工具。
*/

class WpBoxGetMaps{
	var $type;
	var $latlng;
	var $address;
	var $width;
	var $height;
	var $zoom;
	function __construct($type,$latlng,$address,$width,$height,$zoom){
		$this->type=$type;
		$this->latlng=$latlng;
		$this->address=$address;
		$this->width=$width;
		$this->height=$height;
		$this->zoom=$zoom;
	}
	function  returnMap(){
		if ($this->latlng=="" && $this->address==""){
			return "<div><p>无效的坐标或地址。</p></div>";
		}elseif ($this->latlng==""){
			return '<div style="width:'.$this->width.'px;height:'.$this->height.'px;" id="'.$this->type.'"></div>
				<script charset="utf-8"> 				
				var init = function() {
					var geocoder,map,marker = null;
				    var center = new qq.maps.LatLng(39.916527,116.397128);
				    map = new qq.maps.Map(document.getElementById("'.$this->type.'"),{
				        center: center,
				        zoom: 15
				    });
				    geocoder = new qq.maps.Geocoder({
				        complete : function(result){
				            map.setCenter(result.detail.location);
				        }
				    });
				 geocoder.getLocation("'.$this->address.'");
				}
				 
			    if(document.addEventListener){
					window.addEventListener(\'load\',init,false);
				}else{
					window.attachEvent(\'onload\',init);
				}
				</script>';
		}else {
			return '<div style="width:'.$this->width.'px;height:'.$this->height.'px;" id="'.$this->type.'"></div>
				<script charset="utf-8">
				function init() {
				  	var map = new qq.maps.Map(document.getElementById("'.$this->type.'"),{
			        center: new qq.maps.LatLng('.$this->latlng.'),
			        zoom: 13,
			        draggable: true,
			        scrollwheel: true,
			        disableDoubleClickZoom: true
			    	});
				}
			    if(document.addEventListener){
					window.addEventListener(\'load\',init,false);
					}else{
					window.attachEvent(\'onload\',init);
				}
				</script>';
		}
	}
	
	function  returnMapWidget(){
		if ($this->latlng=="" && $this->address==""){
			return "<div><p>无效的坐标或地址。</p></div>";
		}elseif ($this->latlng==""){
			return '<div style="width:'.$this->width.'px;height:'.$this->height.'px;" id="'.$this->type.'"></div>
				<script charset="utf-8">
				var geocoderw,mapw,markerw = null;
				var initw = function() {
				    var centerw = new qq.maps.LatLng(39.916527,116.397128);
				    mapw = new qq.maps.Map(document.getElementById("'.$this->type.'"),{
				        center: centerw,
				        zoom: 15
				    });
				    geocoderw = new qq.maps.Geocoder({
				        complete : function(result){
				            mapw.setCenter(result.detail.location);
				            var markerw = new qq.maps.Marker({
				                map:mapw,
				                position: result.detail.location
				            });
				        }
				    });
				 geocoderw.getLocation("'.$this->address.'");
				}
			    if(document.addEventListener){
					window.addEventListener(\'load\',initw,false);
				}else{
					window.attachEvent(\'onload\',initw);
				}
				</script>';
		}
		else {
			return '<div style="width:'.$this->width.'px;height:'.$this->height.'px;" id="'.$this->type.'"><img src="http://st.map.qq.com/api?size='.$this->width.'*'.$this->height.'&center='.$this->latlng.'&zoom='.$this->zoom.'&markers='.$this->latlng.',1"></img></div>';
		}
	}
}

class WpBoxMaps{	
	function setShortCode($atts){	
		extract(shortcode_atts(array(
		"latlng" => '',
		"address" => '中国,北京,海淀区,故宫博物院',
		"width" => '200',
		"height" => '200',
		"zoom" => '10',
		), $atts));
		$map=new WpBoxGetMaps('wp-box-map-'.get_the_id(),$latlng,$address,$width,$height,$zoom);
		return $map->returnMap();
	}
	function addScript(){
		wp_register_script('plugin', plugins_url('js/qqmap.js?v=2.exp', __FILE__));
		wp_enqueue_script('plugin');
	}
}

class WpBoxMapWidget extends WP_Widget {

	/** 构造函数 */
	function WpBoxMapWidget() {
		parent::WP_Widget(false, $name = 'WP盒子地图');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {
		extract( $args );
		?>
              <?php echo $before_widget; ?>
                  <?php echo $before_title
                      . $instance['title']
                      . $after_title; ?>
              <?php
              	$mapWidhet=new WpBoxGetMaps('wp-box-map-widget',$instance['latlng'], $instance['address'],$instance['width'], $instance['height'], $instance['zoom']);
              	echo $mapWidhet->returnMapWidget();
              ?>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {				
        $title = esc_attr($instance['title'])==''?"WP盒子地图":esc_attr($instance['title']);
        $latlng = esc_attr($instance['latlng'])==''?'116.39782,39.90611':esc_attr($instance['latlng']);
        $address = esc_attr($instance['address'])==''?'':esc_attr($instance['address']);
        $width = esc_attr($instance['width'])==''?'200':esc_attr($instance['width']);
        $height = esc_attr($instance['height'])==''?'200':esc_attr($instance['height']);
        $zoom = esc_attr($instance['zoom'])==''?'16':esc_attr($instance['zoom']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('标题:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id('latlng'); ?>"><?php _e('经纬度:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('latlng'); ?>" name="<?php echo $this->get_field_name('latlng'); ?>" type="text" value="<?php echo $latlng; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('地址:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('宽度:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" type="text" value="<?php echo $width; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('高度:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" type="text" value="<?php echo $height; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id('zoom'); ?>"><?php _e('缩放等级:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('zoom'); ?>" name="<?php echo $this->get_field_name('zoom'); ?>" type="text" value="<?php echo $zoom; ?>" /></label></p>
        <?php 
    }

} // class FooWidget
add_action('widgets_init', create_function('', 'return register_widget("WpBoxMapWidget");'));
add_action('init', 'wpbox_map_button');
function wpbox_map_button() {
	//判断用户是否有编辑文章和页面的权限
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
		return;
	}
	//判断用户是否使用可视化编辑器
	if ( get_user_option('rich_editing') == 'true' ) {

		add_filter( 'mce_external_plugins', 'add_plugin' );
		add_filter( 'mce_buttons', 'register_button' );
	}
}
function register_button( $buttons ) {
	array_push( $buttons, "|", "wpbox_map_button" ); 

	return $buttons;
}
function add_plugin( $plugin_array ) {
	$plugin_array['wpbox_map_button'] = plugins_url('js/map.js', __FILE__); //myadvert按钮的js路径
	return $plugin_array;
}
$WpBoxMaps=new WpBoxMaps();
add_action( 'wp_enqueue_scripts', array(&$WpBoxMaps,'addScript'));
add_shortcode("wpboxmap", array(&$WpBoxMaps, 'setShortCode'));
?>