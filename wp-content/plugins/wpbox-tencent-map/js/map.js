(function() {   
    tinymce.create('tinymce.plugins.myadvert', { //注意这里有个 myadvert   
        init : function(ed, url) {   
            ed.addButton('wpbox_map_button', { //注意这一行有一个 myadvert   
                title : 'WP盒子地图', 
                image : url+'/tmap.png',
                onclick : function() {   
                     ed.selection.setContent('[wpboxmap latlng="39.916527,116.397128" address="中国,北京,故宫" width="500" height="300" zoom="3"][/wpboxmap]'); //这里是你要插入到编辑器的内容，你可以直接写上广告代码  
    
                }   
            });   
        },   
        createControl : function(n, cm) {   
            return null;   
        },   
    });   
    tinymce.PluginManager.add('wpbox_map_button', tinymce.plugins.myadvert); //注意这里有两个 myadvert   
})();  