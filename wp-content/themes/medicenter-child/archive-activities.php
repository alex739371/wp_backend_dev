<?php 
get_header();
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<?php
				if(is_archive())
				{
					if(is_day())
						$archive_header = __("Daily archives: ", 'medicenter') . get_the_date(); 
					else if(is_month())
						$archive_header = __("Monthly archives: ", 'medicenter') . get_the_date('F, Y');
					else if(is_year())
						$archive_header = __("Yearly archives: ", 'medicenter') . get_the_date('Y');
					else
						$archive_header = "Archives";
				}
				?>
				<h1 class="page_title">学术活动 Activities</h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php
						if( is_post_type_archive() ) {
							post_type_archive_title();
						} else {
							echo (is_category() || is_archive() ? (is_category() ? single_cat_title("", false) : $archive_header) : get_the_title());
						}
						?>
					</li>
				</ul>
			</div>
			<?php
			if( 0 ) {
			if(is_category() || is_archive())
			{
				/*get page with blog template set*/
				$post_template_page_array = get_pages(array(
					'post_type' => 'page',
					'post_status' => 'publish',
					'number' => 1,
					'meta_key' => '_wp_page_template',
					'meta_value' => 'template-blog.php',
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
				));
				$post_template_page = $post_template_page_array[0];
				$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			}
			else
				$sidebar = get_post(get_post_meta(get_the_ID(), "page_sidebar_header", true));
			?>
			<div class="page_header_right">
				<?php
				if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name))
					dynamic_sidebar($sidebar->post_name);
				?>
			</div>
			<?php
			}
			?>
		</div>
		<div class="clearfix">
			<div class="activities">
				<div class="date_filters">
					<ul>
						<li<?php if(!$_GET['date_filter']) { ?> class="active"<?php } ?>>
							<a href="<?php echo remove_query_arg('date_filter', get_post_type_archive_link('activities')); ?>">全部活动 / All</a>
						</li>
						<li<?php if('1' == $_GET['date_filter']) { ?> class="active"<?php } ?>>
							<a href="<?php echo add_query_arg('date_filter', '1', get_post_type_archive_link('activities')); ?>">即将开幕 / Coming soon</a>
						</li>	
						<li<?php if('2' == $_GET['date_filter']) { ?> class="active"<?php } ?>>
							<a href="<?php echo add_query_arg('date_filter', '2', get_post_type_archive_link('activities')); ?>">进行中/ In progress</a>
						</li>	
						<li<?php if('3' == $_GET['date_filter']) { ?> class="active"<?php } ?>>
							<a href="<?php echo add_query_arg('date_filter', '3', get_post_type_archive_link('activities')); ?>">往期活动 / Expired</a>
						</li>																		
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="main">
					<ul class="list">
						<?php $i = 1; if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php
						$bi_title = get_post_meta( get_the_ID(), 'bi_title', true );
						$start_date = get_post_meta( get_the_ID(), 'start_date', true );
						$end_date = get_post_meta( get_the_ID(), 'end_date', true );
						$location = get_post_meta( get_the_ID(), 'location', true );
						$host = get_post_meta( get_the_ID(), 'host', true );
						$link = get_post_meta( get_the_ID(), 'link', true );
						$btn_show = get_post_meta( get_the_ID(), 'btn_show', true );
						$btn_txt = get_post_meta( get_the_ID(), 'btn_txt', true );
						$btn_txt = $btn_txt ? $btn_txt : 'BDC会员优惠&nbsp;|&nbsp;BDC members consession';
						$btn_url = get_post_meta( get_the_ID(), 'btn_url', true );

						$current_date = date('ndY');
						$start_date_num = get_post_meta( get_the_ID(), 'start_date_num', true );
						$end_date_num = get_post_meta( get_the_ID(), 'end_date_num', true );
						if($start_date_num > $current_date) {
							$type = 1;
							$text = 'Coming soon';
						} elseif(($start_date_num <= $current_date) && ($end_date_num >= $current_date)) {
							$type = 2;
							$text = 'Ongoing';
						} elseif ($end_date_num < $current_date) {
							$type = 3;
							$text = 'Expired';
						}
						?>
						<li>
							<div class="img">
								<a href="<?php the_permalink(); ?>">
									<?php echo get_the_post_thumbnail( get_the_ID(), 'activities_thumb' ); ?>	
									<span class="type_<?php echo $type; ?>"><?php echo $text; ?></span>	
								</a>					
							</div>		
							<div class="text">
								<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>	
								<ul>
									<?php
									if( $bi_title ) {
										?>
										<li><?php echo $bi_title; ?></li>
										<?php
									}
									?>
									<li>
										<div class="date">
											<?php 
											if( $start_date ) {
												$start_date_arr = explode( '-', $start_date );
												$start_date = date( 'M', mktime( 0, 0, 0, $start_date_arr[0], 10 ) );
												$start_date .= ' ' . $start_date_arr[1] . ordinal_suffix( $start_date_arr[1] ) . ' ';
												$start_date .= ', ' . $start_date_arr[2];
											}
											if( $end_date ) {
												$end_date_arr = explode( '-', $end_date );
												$end_date = date( 'M', mktime( 0, 0, 0, $end_date_arr[0], 10 ) );
												$end_date .= ' ' . $end_date_arr[1] . ordinal_suffix( $end_date_arr[1] ) . ' ';
												$end_date .= ', ' . $end_date_arr[2];
											}											
											?>
											<?php echo $start_date . ' - ' . $end_date; ?>
										</div>
									</li>
									<?php
									if( $location ) {
									?>									
										<li><?php echo $location; ?></li>
									<?php
									}
									?>									
									<?php
									if( $host ) {
									?>
										<li><?php echo $host; ?></li>
									<?php
									}
									?>
									<?php
									if( $link ) {
									?>									
										<li class="link">
											<a href="<?php echo $link; ?>">活动官方网站</a>&nbsp;|&nbsp;<a href="<?php echo $link; ?>">official link</a>
										</li>
									<?php
									}
									?>									
								</ul>				
								<?php
								if( $btn_show ) {
									?>
									<a href="<?php echo $btn_url; ?>" class="button"><?php echo $btn_txt; ?></a>
									<?php
								}
								?>	
							</div>	
							<div class="clearfix"></div>
						</li>
						<?php 
						$i++;
						endwhile; 
						endif; 
						rewind_posts(); 
						wp_reset_query();
						?>						
					</ul>
					<?php 
					$settings = array(
							'range' => '5',
							'before' => '',
							'after' => '',
						); 
					of_pagenavi( $settings ); 
					?>						
				</div>	
				<div class="sidebar">
					<?php if ( is_active_sidebar( 'activities_sidebar' ) ) : ?>
						<?php dynamic_sidebar( 'activities_sidebar' ); ?>
					<?php endif; ?>							
				</div>
				<div class="clearfix"></div>
			</div>					
		</div>		
	</div>
</div>
<?php
get_footer(); 
?>