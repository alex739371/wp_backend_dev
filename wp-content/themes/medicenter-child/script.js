jQuery(document).ready(function($) {
    var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;

    $('.single_slider').each(function() {
        var id = $(this).attr('id').replace('single_slider_', '');
        if($(this).find('.slides').find('li').length>0) {
            $('.post_image[data-id="' + id + '"]').addClass('post_image_a');
        }
    });

    $('.post_image_a').click(function() {
        var slider_id = $(this).attr('data-id'),
            href = $(this).attr('href');
        $('#single_slider_'+slider_id).show().css({left:0, opacity:0}).stop().animate({opacity:1}, 600);
        $.get(href);
        return false;
    });

    $('.single_slider_close').click(function() {
        $('.single_slider').stop().animate({opacity:0}, 300, function() {
            $('.single_slider').css({left:'-9999px'})
        });
        return false;
    });

    $('.single_slider').click(function() {
        $(this).find('.single_slider_close').trigger('click');
        return false;
    });

    $('.single_slider .flexslider').click(function(e) {
        e.stopPropagation();
    });    

    function set_table_size() {
        var width = $(window).width(),
            height = $(window).height();
        width = (width>720)?720:width;
        //$('.table, .td, .single_slider .flexslider, .single_slider .flexslider .slides, .single_slider .flexslider .slides > li, .single_slider .flexslider .slides img').width(width);
        //$('.table, .td').height(height);
        $('.single_slider .flexslider, .single_slider .flexslider .table, .single_slider .flexslider .table .td, .single_slider .flexslider .table .td .flexslider').width(width).height(height);
        $('.img_box').width(width);

        $('.img_box img').css({maxHeight:(height-180)});
        $('.img_box').each(function() {
            var $this = $(this),
                img_width = $this.find('img').width(),
                img_height = $this.find('img').height();
            $this.width(img_width).height(img_height);
        });
    }

    set_table_size();

    $(window).resize(function() {
        //set_table_size();
    });

    function set_slider_pagination($this, current, total) {
        $this.find('.slider_pagination .current').text(current);
        $this.find('.slider_pagination .total').text(total);
    }

    //$(window).load(function () {
        $('.single_slider .flexslider').each(function() {
            var $this = $(this);
            $this.flexslider({
                animation: 'slide',
                slideshow: false,
                slideshowSpeed: 3500,
                animationSpeed: 400,
                pauseOnAction: false,
                pauseOnHover: false,
                start: function(slider) {
                    set_table_size();
                    $(window).resize(function() {
                        set_table_size();
                    });
                    set_slider_pagination($this, slider.currentSlide+1, slider.count); 
                    $this.find('.slider_pagination .prev').click(function() {
                        //$this.find('.flex-prev').trigger('click');
                        //slider.flexAnimate(slider.getTarget('prev'), true);  
                        slider.flexAnimate(slider.getTarget('prev'), true);  
                        return false;
                    });    
                    $this.find('.slider_pagination .next').click(function() {
                        //$this.find('.flex-next').trigger('click');
                        //slider.flexAnimate(slider.getTarget('next'), true);  
                        slider.flexAnimate(slider.getTarget('next'), true);  
                        return false;
                    });  
                    $this.find('.slider_pagination .play').click(function() {
                        if($(this).hasClass('pause')) {
                            $(this).parents('.flexslider').find('.play').removeClass('pause');
                            slider.pause();
                        } else {
                            $(this).parents('.flexslider').find('.play').addClass('pause');
                            slider.play();
                        }
                        return false;
                    });       
                    $this.find('.single_slider_close').click(function() {
                        $(this).parents('.flexslider').find('.play').removeClass('pause');
                        slider.pause();
                        return false;
                    });    

                    if(!supportsTouch) {
                        $('.img_box').hover(function() {
                            var height = $(this).find('.desc_inner').height('auto').height();
                            height = (height<34) ? 34 : height;
                            $(this).find('.desc_inner').height('34px').stop().animate({height:height+'px'}, 400);
                        }, function() {
                            $(this).find('.desc_inner').stop().animate({height:'34px'}, 400);
                        });    
                    } else {
                        $('.img_box').unbind('click').bind('click', function() {
                            if($(this).hasClass('hp')) {
                                $(this).removeClass('hp').find('.desc_inner').stop().animate({height:'34px'}, 400);
                            } else {
                                var height = $(this).addClass('hp').find('.desc_inner').height('auto').height();
                                height = (height<34) ? 34 : height;
                                $(this).find('.desc_inner').height('34px').stop().animate({height:height+'px'}, 400);
                            }
                        });                        
                    }    



                    $('body').bind('keydown',function(e) {
                        if(39==e.keyCode) {
                            $this.find('.flex-next').trigger('click');
                        }
                        if(37==e.keyCode) {
                            $this.find('.flex-prev').trigger('click');
                        }                        
                    });                                                   
                },
                after: function(slider) {
                    set_slider_pagination($this, slider.currentSlide+1, slider.count);             
                }
            });  
        });      
    //});	
});