<?php 
/*
Template Name: Blog
*/
get_header();
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<?php
				if(is_archive())
				{
					if(is_day())
						$archive_header = __("Daily archives: ", 'medicenter') . get_the_date(); 
					else if(is_month())
						$archive_header = __("Monthly archives: ", 'medicenter') . get_the_date('F, Y');
					else if(is_year())
						$archive_header = __("Yearly archives: ", 'medicenter') . get_the_date('Y');
					else
						$archive_header = "Archives";
				}
				?>
				<h1 class="page_title">顶级幻灯片 Top Slides</h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php echo (is_category() || is_archive() ? (is_category() ? single_cat_title("", false) : $archive_header) : get_the_title());?>
					</li>
				</ul>
			</div>
			<?php
			if(is_category() || is_archive())
			{
				/*get page with blog template set*/
				$post_template_page_array = get_pages(array(
					'post_type' => 'page',
					'post_status' => 'publish',
					'number' => 1,
					'meta_key' => '_wp_page_template',
					'meta_value' => 'template-blog.php',
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
				));
				$post_template_page = $post_template_page_array[0];
				$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			}
			else
				$sidebar = get_post(get_post_meta(get_the_ID(), "page_sidebar_header", true));
			?>
			<div class="page_header_right">
				<?php
				if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name))
					dynamic_sidebar($sidebar->post_name);
				?>
			</div>
		</div>

		<div class="clearfix">
			<div class="vc_row wpb_row vc_row-fluid">
				<div class="vc_col-sm-8 wpb_column vc_column_container">
					<div class="wpb_wrapper">
						<ul class="blog clearfix">
							<li class="post-3734 post type-post status-publish format-standard has-post-thumbnail hentry category-notes tag-cancer-oncology tag-liver-disease-hepatitis tag-transplants-organ-donations"><ul class="comment_box clearfix"><li class="date clearfix animated_element animation-slideRight slideRight" style="animation-duration: 600ms; animation-delay: 0ms; transition-delay: 0ms;">
										<div class="value">2015-2-7</div>
										<div class="arrow_date"></div>
									</li><li class="comments_number animated_element animation-slideUp duration-300 delay-500 slideUp" style="animation-duration: 300ms; animation-delay: 500ms; transition-delay: 500ms;">            <a href="#">22 clicks</a>	</li></ul><div class="post_content"><a title="UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发" href="http://localhost/medicenter/ucla-transplant-researchers-develop-novel-method-to-predict-postoperative-liver-cancer-recurrence-in-transplant-patients/" class="post_image"><img width="511" height="280" title="" alt="UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发" class="attachment-blog-post-thumb wp-post-image" src="http://localhost/medicenter/wp-content/uploads/2015/01/Liver.jpg?i=74623" style="display: block;"></a><h2><a title="UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发" href="http://localhost/medicenter/ucla-transplant-researchers-develop-novel-method-to-predict-postoperative-liver-cancer-recurrence-in-transplant-patients/">UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发</a></h2>中文English 你好 I am text block. Click edit button to chan<a class="more" href="http://localhost/medicenter/ucla-transplant-researchers-develop-novel-method-to-predict-postoperative-liver-cancer-recurrence-in-transplant-patients/" title="Read more">Read more →</a>		<div class="post_footer clearfix"><ul class="post_footer_details"><li>Posted in </li><li>
													<a title="View all posts filed under 临床笔记" href="http://localhost/medicenter/category/notes/">临床笔记</a>
												</li>			</ul><ul class="post_footer_details">
											<li>Posted by</li>
											<li>
												
											</li>
										</ul></div></div></li></ul>
				</div> 
			</div> 

			<div class="vc_col-sm-4 wpb_column vc_column_container">
				<div class="wpb_wrapper">
					
			<div class="wpb_widgetised_column wpb_content_element clearfix page_margin_top">
				<div class="wpb_wrapper">
					<div class="widget scrolling_most_viewed_widget sidebar_box" id="medicenter_scrolling_most_viewed-2">		<div class="clearfix">
					<div class="header_left">
						<h3 class="box_header animation-slide animation-slide slide">浏览最多</h3>			</div>
					<div class="header_right">
						<a class="scrolling_list_control_left icon_small_arrow left_black" id="most_viewed_prev" href="#" style="display: block;"></a>
						<a class="scrolling_list_control_right icon_small_arrow right_black" id="most_viewed_next" href="#" style="display: block;"></a>
					</div>
				</div>
				<div class="scrolling_list_wrapper">
					<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 310px; height: 223px; margin: 0px; overflow: hidden;"><ul class="scrolling_list most_viewed" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; height: 1703px; width: 310px;">
										<li class="icon_small_arrow right_black">
							<a title="仅用三种配料合成的可移植的干细胞器" class="clearfix" href="http://localhost/medicenter/%e4%bb%85%e7%94%a8%e4%b8%89%e7%a7%8d%e9%85%8d%e6%96%99%e5%90%88%e6%88%90%e7%9a%84%e5%8f%af%e7%a7%bb%e6%a4%8d%e7%9a%84%e8%82%9d%e7%bb%86%e8%83%9e%e5%99%a8/">
								<span class="left">
									仅用三种配料合成的可移植的干细胞器						</span>
								<span class="number">
									436						</span>
							</a>
							<abbr class="timeago" title="2014-10-17T09:09:57+0800">5 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="L4/5 腰椎退行性滑脱伴L5硬膜内髓外肿瘤" class="clearfix" href="http://localhost/medicenter/l45/">
								<span class="left">
									L4/5 腰椎退行性滑脱伴L5硬膜内髓外肿瘤						</span>
								<span class="number">
									120						</span>
							</a>
							<abbr class="timeago" title="2014-12-06T23:43:36+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="颈椎前路手术治疗脊髓型颈椎病（C5/6, C6/7）" class="clearfix" href="http://localhost/medicenter/c5-6-c6-7/">
								<span class="left">
									颈椎前路手术治疗脊髓型颈椎病（C5/6, C6/7）						</span>
								<span class="number">
									30						</span>
							</a>
							<abbr class="timeago" title="2014-12-08T21:50:49+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="脑血管概述" class="clearfix" href="http://localhost/medicenter/%e8%84%91%e8%a1%80%e7%ae%a1%e6%a6%82%e8%bf%b0/">
								<span class="left">
									脑血管概述						</span>
								<span class="number">
									25						</span>
							</a>
							<abbr class="timeago" title="2014-12-15T21:48:29+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发" class="clearfix" href="http://localhost/medicenter/ucla-transplant-researchers-develop-novel-method-to-predict-postoperative-liver-cancer-recurrence-in-transplant-patients/">
								<span class="left">
									UCLA 移植研究团队利用新方法预测肝移植后病人的肝癌复发						</span>
								<span class="number">
									22						</span>
							</a>
							<abbr class="timeago" title="2015-02-07T15:23:27+0800">about a month ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="双侧椎板减压术后路（PLIF）+椎弓根螺钉固定术（PS）治疗腰椎滑脱症" class="clearfix" href="http://localhost/medicenter/%e5%8f%8c%e4%be%a7%e6%a4%8e%e6%9d%bf%e5%87%8f%e5%8e%8b%e6%9c%af%e5%90%8e%e8%b7%af%ef%bc%88plif%ef%bc%89%e6%a4%8e%e5%bc%93%e6%a0%b9%e8%9e%ba%e9%92%89%e5%9b%ba%e5%ae%9a%e6%9c%af%ef%bc%88ps%ef%bc%89/">
								<span class="left">
									双侧椎板减压术后路（PLIF）+椎弓根螺钉固定术（PS）治疗腰椎滑脱症						</span>
								<span class="number">
									18						</span>
							</a>
							<abbr class="timeago" title="2014-12-14T13:06:04+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="test" class="clearfix" href="http://localhost/medicenter/test-3/">
								<span class="left">
									test						</span>
								<span class="number">
									17						</span>
							</a>
							<abbr class="timeago" title="2014-12-07T13:01:05+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="今日文章" class="clearfix" href="http://localhost/medicenter/%e4%bb%8a%e6%97%a5%e6%96%87%e7%ab%a0/">
								<span class="left">
									今日文章						</span>
								<span class="number">
									13						</span>
							</a>
							<abbr class="timeago" title="2015-01-21T00:03:04+0800">2 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="腰椎压缩性骨折-原版手术记录" class="clearfix" href="http://localhost/medicenter/%e8%85%b0%e6%a4%8e%e5%8e%8b%e7%bc%a9%e6%80%a7%e9%aa%a8%e6%8a%98-%e5%8e%9f%e7%89%88%e6%89%8b%e6%9c%af%e8%ae%b0%e5%bd%95/">
								<span class="left">
									腰椎压缩性骨折-原版手术记录						</span>
								<span class="number">
									11						</span>
							</a>
							<abbr class="timeago" title="2014-12-09T23:06:32+0800">3 months ago</abbr>
						</li>
										<li class="icon_small_arrow right_black">
							<a title="后交通动脉瘤-夹闭手术" class="clearfix" href="http://localhost/medicenter/%e5%90%8e%e4%ba%a4%e9%80%9a%e5%8a%a8%e8%84%89%e7%98%a4-%e5%a4%b9%e9%97%ad%e6%89%8b%e6%9c%af/">
								<span class="left">
									后交通动脉瘤-夹闭手术						</span>
								<span class="number">
									11						</span>
							</a>
							<abbr class="timeago" title="2014-12-10T18:43:57+0800">3 months ago</abbr>
						</li>
									</ul></div>
				</div>
				</div>
				</div> 
			</div> 
			<div class="wpb_widgetised_column wpb_content_element clearfix page_margin_top_section">
				<div class="wpb_wrapper">
					
				</div> 
			</div> 
				</div> 
			</div> 
		</div>
		</div>	
	</div>
</div>
<?php
get_footer(); 
?>