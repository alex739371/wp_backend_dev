<?php 
get_header();
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<?php
				if(is_archive())
				{
					if(is_day())
						$archive_header = __("Daily archives: ", 'medicenter') . get_the_date(); 
					else if(is_month())
						$archive_header = __("Monthly archives: ", 'medicenter') . get_the_date('F, Y');
					else if(is_year())
						$archive_header = __("Yearly archives: ", 'medicenter') . get_the_date('Y');
					else
						$archive_header = "Archives";
				}
				?>
				<h1 class="page_title">疾病查询 Disease Base</h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php
						if( is_post_type_archive() ) {
							post_type_archive_title();
						} else {
							echo (is_category() || is_archive() ? (is_category() ? single_cat_title("", false) : $archive_header) : get_the_title());
						}
						?>
					</li>
				</ul>
			</div>
			<?php
			if( 0 ) {
			if(is_category() || is_archive())
			{
				/*get page with blog template set*/
				$post_template_page_array = get_pages(array(
					'post_type' => 'page',
					'post_status' => 'publish',
					'number' => 1,
					'meta_key' => '_wp_page_template',
					'meta_value' => 'template-blog.php',
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
				));
				$post_template_page = $post_template_page_array[0];
				$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			}
			else
				$sidebar = get_post(get_post_meta(get_the_ID(), "page_sidebar_header", true));
			?>
			<div class="page_header_right">
				<?php
				if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name))
					dynamic_sidebar($sidebar->post_name);
				?>
			</div>
			<?php
			}
			?>
		</div>
		<div class="clearfix">
			<div class="vc_row wpb_row vc_row-fluid ">
				<?php $i = 1; if (have_posts()) : while (have_posts()) : the_post(); ?>
			    <div class="pe_box vc_col-sm-4 wpb_column vc_column_container <?php if( 1==$i%3 ) { echo 'pe_no_ml'; } ?>">
			        <div class="wpb_wrapper">
			            <div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_bottom-to-top vc_align_center">
			                <div class="wpb_wrapper">
			                    <a href="<?php the_permalink(); ?>">
			                        <?php echo get_the_post_thumbnail( get_the_ID(), 'grid_thumb' ); ?>
			                    </a>
			                </div>
			            </div>
			            <div class="wpb_text_column wpb_content_element  vc_custom_1427127287391 wpb_animate_when_almost_visible wpb_top-to-bottom">
			                <div class="wpb_wrapper">
			                    <a href="<?php the_permalink(); ?>" class="pe_link"><?php the_title(); ?></a>
			                </div>
			            </div>
			        </div>
			    </div>
				<?php 
				$i++;
				endwhile; 
				endif; 
				rewind_posts(); 
				wp_reset_query();
				?>			    			    
			</div>		
			<?php 
			$settings = array(
					'range' => '5',
					'before' => '',
					'after' => '',
				); 
			of_pagenavi( $settings ); 
			?>								
		</div>		
	</div>
</div>


<link rel='stylesheet' id='js_composer_front-css'  href='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/css/js_composer.css?ver=4.3.5' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_custom_css-css'  href='<?php bloginfo( 'url' ); ?>/wp-content/uploads/js_composer/custom.css?ver=4.3.5' type='text/css' media='screen' />
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/js/js_composer_front.js?ver=4.3.5'></script>
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/lib/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js?ver=4.3.5'></script>
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/lib/jquery-waypoints/waypoints.min.js?ver=4.3.5'></script>
<style type="text/css" data-type="vc_shortcodes-custom-css">
.vc_custom_1423931572000{padding-top: 10px !important;}.vc_custom_1423931742245{padding-top: 10px !important;padding-bottom: 20px !important;}body .vc_custom_1427127287391{background-color: #eaeaea !important;margin-bottom: 30px;}.vc_custom_1427254938543{background-color: #eaeaea !important;}
.wpb_column {
	margin-bottom: 30px;
}
</style>	

<?php
get_footer(); 
?>