<?php
/*
Template Name: Single post
*/
get_header();
setPostViews(get_the_ID());
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<h1 class="page_title"><?php the_title(); ?></h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php the_title(); ?>
					</li>
				</ul>
			</div>
			<?php
			/*get page with single post template set*/
			$post_template_page_array = get_pages(array(
				'post_type' => 'page',
				'post_status' => 'publish',
				//'number' => 1,
				'meta_key' => '_wp_page_template',
				'meta_value' => 'single.php'
			));
			$post_template_page = $post_template_page_array[0];
			$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name)):
			?>
			<div class="page_header_right">
				<?php
				dynamic_sidebar($sidebar->post_name);
				?>
			</div>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix">
			<?php
				//get_template_part( 'cp_gallery_box' );	
			?>			
			<?php
			if(count($post_template_page_array) && isset($post_template_page))
			{
				echo wpb_js_remove_wpautop(apply_filters('the_content', $post_template_page->post_content));
				global $post;
				$post = $post_template_page;
				setup_postdata($post);
			}
			else
				echo wpb_js_remove_wpautop(apply_filters('the_content', '[vc_row top_margin="none" el_position="first last"] [vc_column top_margin="none" width="2/3"] [single_post featured_image_size="default" columns="1" show_post_categories="1" show_post_author="1" comments="1" lightbox_icon_color="blue_light" el_position="first last"] [/vc_column] [vc_column top_margin="none" width="1/3"] [vc_widget_sidebar sidebar_id="sidebar-blog" top_margin="page_margin_top" el_position="first"] [box_header title="Photostream" type="h3" bottom_border="1" top_margin="page_margin_top_section"] [photostream id="photostream" images="21,15,16,17,18,19" featured_image_size="default" lightbox_icon_color="blue_light" images_loop="1" top_margin="none"] [vc_widget_sidebar sidebar_id="sidebar-blog-2" top_margin="page_margin_top_section" el_position="last"] [/vc_column] [/vc_row]'));
			?>
		</div>
	</div>
</div>

<?php
wp_reset_query();
if( 'top_slides'==get_post_type() ) {
	?>
<div class="single_slider" id="single_slider_<?php wp_reset_query(); global $post; echo $post->ID; ?>">	
	<div class="flexslider">
		<ul class="slides">
			<?php
				wp_reset_query();
				global $post;
				$slides = get_post_meta( $post->ID, 'slides', true );
				if( $slides > 0 ) {
					for ($i=($slides); $i >= 0; $i--) { 
						$img = get_post_meta( $post->ID, 'slides_' . $i . '_img', true );
						$desc = get_post_meta( $post->ID, 'slides_' . $i . '_desc', true );
						if( $img ) {
						?>
			            <li>
							<div class="table">
								<div class="td">		
									<div class="img_box">		            	
						            	<img src="<?php echo wp_get_attachment_url( $img ); ?>" alt="" />
						            	<div class="desc">
						            		<div class="desc_inner">
						            		<?php echo wpautop( $desc ); ?>
						            		</div>
						            	</div>
						            	<a href="#" class="single_slider_close"></a>
										<div class="slider_pagination">
											<a href="#" class="play"></a>
											<a href="#" class="prev"></a>
											<a href="#" class="next"></a>
											<div class="inner"><span class="current"></span> / <span class="total"></span></div>
										</div>						            	
									</div>	
								</div>	
							</div>						            	
			            </li>									
						<?php
						}
					}	
				}	
			?>				
		</ul>
	</div>	
</div>
	<?php
}
?>

<?php
get_footer();
?>