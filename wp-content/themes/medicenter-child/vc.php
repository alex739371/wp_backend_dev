<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1425795524649{border-top-width: 1px !important;border-right-width: 1px !important;border-bottom-width: 1px !important;border-left-width: 1px !important;padding-top: 0px !important;padding-right: 12px !important;padding-bottom: 0px !important;padding-left: 12px !important;border-left-color: #777777 !important;border-left-style: solid !important;border-right-color: #777777 !important;border-right-style: solid !important;border-top-color: #777777 !important;border-top-style: solid !important;border-bottom-color: #777777 !important;border-bottom-style: solid !important;}</style>	


        <div class="wpb_wrapper">
<?php
if( 0 ) {
?>        	
            <div class="vc_empty_space" style="height: 18px">
            	<span class="vc_empty_space_inner"></span>
            </div>
<?php
}
?>
<?php
wp_reset_query();
?>

<?php
if( 0 ) {
?>
            <div class="wpb_text_column wpb_content_element  vc_custom_1425795524649">
                <div class="wpb_wrapper">
					<?php
						global $post;
						$disclaimer = get_post_meta( $post->ID, 'disclaimer', true );
						echo wpautop( $disclaimer );
					?>                	
                </div>
            </div>
<?php
}
?>

			<?php
				//get_template_part( 'cp_gallery_box' );	
			?>


            <div class="clearfix tabs page_margin_top" data-interval="0" style="margin-top:0;">
                <ul class="tabs_navigation clearfix">
					<?php
						global $post;
						$zh_list = get_post_meta( $post->ID, 'zh_list', true );
						if( $zh_list > 0 ) {
						?>
	                    <li>
	                    	<a href="#1424123424-1-48">中文</a>
	                    </li>						
						<?php	
						}	
					?> 
					<?php
						global $post;
						$en_list = get_post_meta( $post->ID, 'en_list', true );
						if( $en_list > 0 ) {
						?>
	                    <li>
	                    	<a href="#1424163358083-2-5">English</a>
	                    </li>					
						<?php	
						}	
					?> 	
                </ul>

                <div id="1424123424-1-48" class="wpb_tab">

<?php
if( 0 ) {
?>
                    <div class="vc_empty_space" style="height: 18px">
                    	<span class="vc_empty_space_inner"></span>
                    </div>

                    <div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_center">
                        <div class="wpb_wrapper">
                            <?php echo get_the_post_thumbnail( get_the_ID(), 'grid_thumb' ); ?>
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
                        <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                        <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
<?php
}
?>





					<?php
						global $post;
						$zh_list = get_post_meta( $post->ID, 'zh_list', true );
						if( $zh_list > 0 ) {
							for ($i=0; $i < $zh_list; $i++) { 
								$title = get_post_meta( $post->ID, 'zh_list_' . $i . '_title', true );
								$img = get_post_meta( $post->ID, 'zh_list_' . $i . '_img', true );
								$desc = get_post_meta( $post->ID, 'zh_list_' . $i . '_desc', true );
								?>
				                    <div class="vc_empty_space" style="height: 18px">
				                    	<span class="vc_empty_space_inner"></span>
				                    </div>	
				                    <?php
				                    if( $title ) {
				                    	?>
				                    	<h3 class="box_header animation-slide"><?php echo $title; ?></h3>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $img ) {
				                    	?>
						                    <div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_center">
						                        <div class="wpb_wrapper">
						                            <img src="<?php echo wp_get_attachment_url( $img ); ?>" class=" vc_box_border_grey attachment-full" alt="" />
						                        </div>
						                    </div>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $desc ) {
				                    	?>
					                    <div class="wpb_text_column wpb_content_element ">
					                        <div class="wpb_wrapper">
					                            <?php echo wpautop( do_shortcode( $desc ) ); ?>
					                        </div>
					                    </div>
				                    	<?php
				                    }
				                    ?>		
				                    <div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
				                        <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
				                        <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
				                    </div>			
								<?php
							}	
						}	
					?>



                </div>
                <div id="1424163358083-2-5" class="wpb_tab">
            <div class="vc_empty_space" style="height: 18px">
            	<span class="vc_empty_space_inner"></span>
            </div>

					<?php
						global $post;
						$en_list = get_post_meta( $post->ID, 'en_list', true );
						if( $en_list > 0 ) {
							for ($i=0; $i < $en_list; $i++) { 
								$title = get_post_meta( $post->ID, 'en_list_' . $i . '_title', true );
								$img = get_post_meta( $post->ID, 'en_list_' . $i . '_img', true );
								$desc = get_post_meta( $post->ID, 'en_list_' . $i . '_desc', true );
								?>
				                    <?php
				                    if( $title ) {
				                    	?>
				                    	<h3 class="box_header animation-slide"><?php echo $title; ?></h3>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $img ) {
				                    	?>
						                    <div class="wpb_text_column wpb_content_element">
						                        <div class="wpb_wrapper">
						                            <img src="<?php echo wp_get_attachment_url( $img ); ?>" class=" vc_box_border_grey attachment-full" alt="" />
						                        </div>
						                    </div>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $desc ) {
				                    	?>
					                    <div class="wpb_text_column wpb_content_element ">
					                        <div class="wpb_wrapper">
					                            <?php echo wpautop( do_shortcode( $desc ) ); ?>
					                        </div>
					                    </div>
				                    	<?php
				                    }
				                    ?>				
								<?php
							}	
						}	
					?>

                </div>
            </div>
        </div>