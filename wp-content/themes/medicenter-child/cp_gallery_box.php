<div class="post_content">
<?php
wp_reset_query();
				global $themename;
				$output = '';
				$show_images_in = get_post_meta(get_the_ID(), $themename . "_show_images_in", true);
				$attachment_ids = get_post_meta(get_the_ID(), $themename . "_attachment_ids", true);
				$images = get_post_meta(get_the_ID(), $themename . "_images", true);
				$images_count = count((array)$images);
				if($images_count>0 && ($show_images_in=="post" || $show_images_in=="both"))
				{
					$images_titles = get_post_meta(get_the_ID(), $themename . "_images_titles", true);
					$videos = get_post_meta(get_the_ID(), $themename . "_videos", true);
					$iframes = get_post_meta(get_the_ID(), $themename . "_iframes", true);
					$external_urls = get_post_meta(get_the_ID(), $themename . "_external_urls", true);
					$features_images_loop = get_post_meta(get_the_ID(), $themename . "_features_images_loop", true);
					$output .= '<div class="gallery_box">
						<ul class="image_carousel">';
						if(has_post_thumbnail())
						{
							$thumb_id = get_post_thumbnail_id(get_the_ID());
							$attachment_image = wp_get_attachment_image_src($thumb_id, "large");
							$large_image_url = $attachment_image[0];
							$thumbnail_image = get_posts(array('p' => $thumb_id, 'post_type' => 'attachment'));
							$output .= '<li><span class="mc_preloader"></span>
							' . get_the_post_thumbnail(get_the_ID(), ($featured_image_size!="default" ? $featured_image_size : ($columns==2 ? $themename . "-gallery-image" : "blog-post-thumb")), array("alt" => get_the_title(), "title" => "")) . '
								<ul class="controls">
									<li>
										<a href="' . $large_image_url . '" title="' . esc_attr($thumbnail_image[0]->post_title) . '" class="fancybox open_lightbox"' . ($features_images_loop=="yes" ? ' rel="featured_' . get_the_ID() . '"' : '') . ' style="background-image: url(\'' . get_stylesheet_directory_uri() . '/images/icons_media/' . $lightbox_icon_color . '/image.png\')"></a>
									</li>
								</ul>
							</li>';
						}
					for($j=0; $j<$images_count; $j++)
					{
						$output .= '<li>' . ($j==0 && !has_post_thumbnail() ? '<span class="mc_preloader"></span>' : '') .
							wp_get_attachment_image((int)$attachment_ids[$j], ($featured_image_size!="default" ? $featured_image_size : ($columns==2 ? $themename . "-gallery-image" : "blog-post-thumb")), array("alt "=> "")) . '
								<ul class="controls">
									<li>
										<a' . ($external_urls[$j]!="" ? ' target="_blank"' : '') . ' href="' . ($external_urls[$j]!="" ? $external_urls[$j] : ($iframes[$j]!="" ? $iframes[$j] : ($videos[$j]!="" ? $videos[$j] : $images[$j] ))) . '" title="' . esc_attr($images_titles[$j]) . '" class="fancybox' . ($external_urls[$j]!="" ? '-externalurl' : ($iframes[$j]!="" ? '-iframe' : ($videos[$j]!="" ? '-video' : '' ))) . ' open_' . ($external_urls[$j]!="" || $iframes[$j]!="" ? 'iframe_' : ($videos[$j]!="" ? 'video_' : '' )) . 'lightbox"' . ($features_images_loop=="yes" ? ' rel="featured_' . get_the_ID() . '"' : '') . ' style="background-image: url(\'' . get_stylesheet_directory_uri() . '/images/icons_media/' . $lightbox_icon_color . '/' . ($videos[$j]!="" ? 'video' : ($iframes[$j]!="" || $external_urls[$j]!="" ? 'url' : 'image')) . '.png\')"></a>
									</li>
								</ul>
							</li>';
					}
					$output .= '</ul>
					</div>';
				}
				echo $output;		
?>
</div>