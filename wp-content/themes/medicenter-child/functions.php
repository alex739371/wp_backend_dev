<?php
// Define New Post Type
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'top_slides', array(
		'labels' => array(
			'name' => __( '顶级幻灯片 Top Slides' ),
			'singular_name' => __( '顶级幻灯片' ),
			'add_new' => __( '添加顶级幻灯片' ),
			'add_new_item' => __( '添加顶级幻灯片' ),
			'edit_item' => __( '编辑顶级幻灯片' ),
			'new_item' => __( '新顶级幻灯片' ),
			'view_item' => __( '查看顶级幻灯片' ),
			'search_item' => __( '搜索顶级幻灯片' ),
			'parent_item_colon' => __( '父级顶级幻灯片' ),
			'not_found' => __( '没有找到' ),
		),
		'menu_position' => 5,
		'public' => true,
		'has_archive' => true,
		'hierarchical' => false, // Whether the post type is hierarchical. Allows Parent to be specified
		'rewrite' => array( 'slug' => 'top-slides' ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' )
	) );	

	register_post_type( 'patient_education', array(
		'labels' => array(
			'name' => __( '疾病查询 Disease Base' ),
			'singular_name' => __( '疾病库' ),
			'add_new' => __( '添加疾病' ),
			'add_new_item' => __( '添加疾病' ),
			'edit_item' => __( '编辑疾病' ),
			'new_item' => __( '新疾病' ),
			'view_item' => __( '查看疾病' ),
			'search_item' => __( '搜索疾病' ),
			'parent_item_colon' => __( '父级疾病' ),
			'not_found' => __( '没有找到' ),
		),
		'menu_position' => 5,
		'public' => true,
		'has_archive' => true,
		'hierarchical' => false, // Whether the post type is hierarchical. Allows Parent to be specified
		'rewrite' => array( 'slug' => 'patient-education' ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' )
	) );	

	register_post_type( 'activities', array(
		'labels' => array(
			'name' => __( '学术活动 Activities' ),
			'singular_name' => __( '学术活动' ),
			'add_new' => __( '添加学术活动' ),
			'add_new_item' => __( '添加学术活动' ),
			'edit_item' => __( '编辑学术活动' ),
			'new_item' => __( '新学术活动' ),
			'view_item' => __( '查看学术活动' ),
			'search_item' => __( '搜索学术活动' ),
			'parent_item_colon' => __( '父级学术活动' ),
			'not_found' => __( '没有找到' ),
		),
		'menu_position' => 5,
		'public' => true,
		'has_archive' => true,
		'hierarchical' => false, // Whether the post type is hierarchical. Allows Parent to be specified
		'rewrite' => array( 'slug' => 'activities' ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments' )
	) );		
}

function my_enqueue_scripts_frontpage() {
	wp_enqueue_script( 'flexsliderjs',  	  	  get_stylesheet_directory_uri() . '/js/jquery.flexslider-min.js' );	
	wp_enqueue_script( 'scriptjs',              get_stylesheet_directory_uri() . '/js/script.js', array(), false, true );

	wp_deregister_style( 'flexslider', vc_asset_url( 'lib/flexslider/flexslider.css' ), false, WPB_VC_VERSION, 'screen' );
	wp_dequeue_script( 'flexslider', vc_asset_url( 'lib/flexslider/jquery.flexslider-min.js' ), array( 'jquery' ), WPB_VC_VERSION, true );
}
add_action( 'wp_footer', 'my_enqueue_scripts_frontpage' );

if ( function_exists( 'add_theme_support' ) ) {
	add_image_size( 'grid_thumb', 310, 276, true );
	add_image_size( 'activities_thumb', 217, 200, true );	
}

function add_custom_body_class( $classes ) {
    if ( is_post_type_archive( 'patient_education' ) ) {
    	$classes[] = 'grid_archive';
    }  

    if( is_singular() && ( 'top_slides'==get_post_type() || 'patient_education'==get_post_type() ) ) {
    	$classes[] = 'single_ppt';
    }   

    return $classes;
}
add_filter( 'body_class', 'add_custom_body_class');

function of_pagenavi( $settings ) {
	$s = wp_parse_args( $settings, array(
		'range' => '5',
		'before' => '',
		'after' => '',
	) );
	$range = intval( $s['range'] );
	global $paged, $wp_query;
	if ( ! $max_page ) {
		$max_page = $wp_query->max_num_pages;
	}
	if( $max_page > 1 ) {
		echo $s['before'];
		echo '<ul class="pagination wp_pagination">';
		//echo '<span class="pages">Pages: </span>';
		if( ! $paged ) {
			$paged = 1;
		}
		if( $paged != 1 ) {
			//echo '<a href="' . get_pagenum_link(1) . '" class="extend first"> Prev </a>';
		}
		previous_posts_link( '' );
		if( $max_page > $range ) {
			if( $paged < $range ) {
				for( $i = 1; $i <= ( $range + 1 ); $i++ ) {
					$classes = array( 'page' );
					$href = get_pagenum_link($i);
					if( $i == $paged )
						$classes[] = 'active';
					printf( '<li><a href="%s" class="%s">%s</a></li>', $href, implode( ' ', $classes ), $i );
				}
				echo '<li><span class="extend">...</span></li>';
			} elseif( $paged >= ( $max_page - ceil( ( $range/2 ) ) ) ) {
				echo '<li><span class="extend">...</span></li>';
				for( $i = $max_page - $range; $i <= $max_page; $i++ ) {
					$classes = array( 'page' );
					$href = get_pagenum_link($i);
					if( $i == $paged )
						$classes[] = 'active';
					printf( '<li><a href="%s" class="%s">%s</a></li>', $href, implode( ' ', $classes ), $i );
				}
			} elseif( $paged >= $range && $paged < ( $max_page - ceil( ( $range/2 ) ) ) ) {
				echo '<li><span class="extend">...</span>';
				for( $i = ( $paged - ceil( $range/2 ) ); $i <= ( $paged + ceil( ( $range/2 ) ) ); $i++ ) {
					$classes = array( 'page' );
					$href = get_pagenum_link($i);
					if( $i == $paged )
						$classes[] = 'active';
					printf( '<li><a href="%s" class="%s">%s</a></li>', $href, implode( ' ', $classes ), $i );
				}
				echo '<li><span class="extend">...</span></li>';
			}
		} else {
			for( $i = 1; $i <= $max_page; $i++ ) {
				$classes = array( 'page' );
				$href = get_pagenum_link($i);
				if( $i == $paged )
					$classes[] = 'active';
				printf( '<li><a href="%s" class="%s">%s</a></li>', $href, implode( ' ', $classes ), $i );
			}
		}
		next_posts_link( '' );
		if( $paged != $max_page ) {
			//echo '<a href="' . get_pagenum_link($max_page) . '" class="extend last"> Next </a>';
		}
		echo '</ul><!-- page_navi -->';
		echo $s['after'];
	}
}

function of_previous_posts_link_attributes() {
	return 'class="prev"';
}
add_filter( 'previous_posts_link_attributes', 'of_previous_posts_link_attributes' );

function of_next_posts_link_attributes() {
	return 'class="next"';
}
add_filter( 'next_posts_link_attributes', 'of_next_posts_link_attributes' );

if( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Activities Sidebar', 
		'id'   => 'activities_sidebar',
		'before_widget' => '<div class="widget widget_archive sidebar_box">', 
		'after_widget' => '</div><!-- widget -->', 
		'before_title' => '<h3 class="box_header">', 
		'after_title' => '</h3><!-- box_header -->' 
	));
}

if( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Top Slides Single Sidebar', 
		'id'   => 'pe_s_sidebar',
		'before_widget' => '<div class="widget widget_archive sidebar_box">', 
		'after_widget' => '</div><!-- widget -->', 
		'before_title' => '<h3 class="box_header">', 
		'after_title' => '</h3><!-- box_header -->' 
	));
}

if( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Patient Education Single Sidebar', 
		'id'   => 'pe_s_sidebar',
		'before_widget' => '<div class="widget widget_archive sidebar_box">', 
		'after_widget' => '</div><!-- widget -->', 
		'before_title' => '<h3 class="box_header">', 
		'after_title' => '</h3><!-- box_header -->' 
	));
}

if( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Activities Single Sidebar', 
		'id'   => 'activities_s_sidebar',
		'before_widget' => '<div class="widget widget_archive sidebar_box">', 
		'after_widget' => '</div><!-- widget -->', 
		'before_title' => '<h3 class="box_header">', 
		'after_title' => '</h3><!-- box_header -->' 
	));
}


function ordinal_suffix($num){
    $num = $num % 100; // protect against large numbers
    if($num < 11 || $num > 13){
         switch($num % 10){
            case 1: return 'st';
            case 2: return 'nd';
            case 3: return 'rd';
        }
    }
    return 'th';
}  

require_once(locate_template("shortcodes/top_slides.php"));

$menus = array(
	'pe_nav'  => 'Patient Education Navigation',
);
register_nav_menus($menus);

function display_favicon() {
	?>
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />		
	<?php
}
add_action( 'wp_head', 'display_favicon' );




function cp_theme_add_custom_box() 
{
	global $themename;
	add_meta_box( 
        "options",
        __("Options", 'medicenter'),
        "theme_inner_custom_box_post",
        "activities",
		"normal",
		"high"
    );
/*
	add_meta_box( 
        "options",
        __("Options", 'medicenter'),
        "theme_inner_custom_box_post",
        "patient_education",
		"normal",
		"high"
    );

	add_meta_box( 
        "options",
        __("Options", 'medicenter'),
        "theme_inner_custom_box_post",
        "top_slides",
		"normal",
		"high"
    );     
*/       
}
add_action("add_meta_boxes", "cp_theme_add_custom_box");





class McMapWidget extends WP_Widget {
    
    // Instantiate the parent object
    function McMapWidget() {
        $widget_ops = array(
            'classname' => 'bs_widget',
            'description' => ''
            );
        parent::__construct( false, '地图', $widget_ops );
    }

    // Widget output
    function widget( $args, $instance ) {
        global $shortname;
        extract($args);
        $title = apply_filters( 'widget_title', empty($instance['title']) ? '&nbsp;' : $instance['title'] );
        $bs_title = empty( $instance['bs_title'] ) ? '' : $instance['bs_title'];
        $bs_subtitle = empty( $instance['bs_subtitle'] ) ? '' : $instance['bs_subtitle'];

        
        //echo $before_title . $title . $after_title;
        if( is_singular() && 'activities'==get_post_type() ) {
        	global $post;
        	$location = get_post_meta( $post->ID, 'location', true );
        	$l_l = get_post_meta( $post->ID, 'l_l', true );
        	echo '<h3 class="box_header animation-slide animation-slide" style="margin-bottom:-30px;">举办地点</h3>';
        	echo $before_widget;
	        echo do_shortcode( '[wpboxmap latlng="' . $l_l . '" address="' . $location . '" width="310" height="300" zoom="3"][/wpboxmap]' );
	        echo $after_widget;
    	}
    }
    
    // Save widget options
    //function update( $new_instance, $old_instance ) {
    //}

    // admin options form
    function form( $instance ) {
        // Default Value
        $instance = wp_parse_args( (array)$instance, array(
            'title' => '',
            )
        );

        $title = htmlspecialchars( $instance['title'] );
        ?>   
                         
        <?php
    }
}

function myall_register_widgets() {
    register_widget( 'McMapWidget' );   
}
add_action( 'widgets_init', 'myall_register_widgets' );

function show_all_posts( $query ) {
    if ( is_post_type_archive( 'patient_education' ) ) {
        $query->set( 'posts_per_page', '-1') ;
    };
    return $query;
};

add_filter( 'pre_get_posts', 'show_all_posts' );

function add_hm() {
	?>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?a3926a54f9f979fd76a066700732f5db";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>	
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.meanmenu.min.js"></script>
<link rel='stylesheet' href='<?php echo get_stylesheet_directory_uri(); ?>/js/meanmenu.css' type='text/css' media='all' />
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#menu-homemenu').parent().meanmenu({
		meanRevealPositionDistance: 0,
		meanScreenWidth: 767
	});
	$('#menu-homemenu').parents('.header_container').addClass('header_container_line');
});
</script>
	<?php
}
add_action( 'wp_head', 'add_hm' );

function ie8_css() {
	?>
<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/ie8.css" type="text/css" media="screen" />
<![endif]-->	
	<?php
}
add_action( 'wp_head', 'ie8_css' );

function ie8_js() {
	?>
<!--[if IE 8]>
	<script type="text/javascript">

  window.addEventListener = function(element, event, fn) {

    // Use addEventListener if available
    if (element.addEventListener) {
      element.addEventListener(event, fn, false);

    // Otherwise use attachEvent, set this and event
    } else if (element.attachEvent) {
      element.attachEvent('on' + event, (function (el) {
        return function() {
          fn.call(el, window.event);
        };
      }(element)));

      // Break closure and primary circular reference to element
      element = null;
    }
  }

jQuery(document).ready(function($) {  
    $('.tabs_navigation a').click(function() {
    	$('.ui-tabs-active').removeClass('ui-tabs-active');
        var index = $(this).parent('li').addClass('ui-tabs-active').index();
        $(this).parents('.tabs').find('.wpb_tab').hide();
        $(this).parents('.tabs').find('.wpb_tab').eq(index).show();
    });
});
	</script>	
<![endif]-->		
	<?php
}
add_action( 'wp_head', 'ie8_js' );




function post_save_num_date( $post_id ) {
	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}
	save_num_date($post_id);
	update_activity_order_status(1);	
}
add_action( 'save_post', 'post_save_num_date' );

function save_num_date($post_id='') {
	$i = 1;
	$query = new WP_Query();
	$args = array(
	    'post_type' => 'activities',
	    'showposts' => '-1', 	
		);
	if($post_id > 0) {
		$args['p'] = $post_id;
	}
	$query->query($args);  
	if($query->have_posts()) : while($query->have_posts()): $query->the_post(); 
	global $post;
	$start_date = get_post_meta( $post->ID, 'start_date', true );
	$end_date = get_post_meta( $post->ID, 'end_date', true );
		$arr = explode('-', $start_date);
		if(1 == strlen($arr[1])) {
			$arr[1] = '0' . $arr[1];
		}
		$start_date = implode('-', $arr);
		$arr = explode('-', $end_date);
		if(1 == strlen($arr[1])) {
			$arr[1] = '0' . $arr[1];
		}
		$end_date = implode('-', $arr);		
		$start_date_num	= str_replace('-', '', $start_date);
		$end_date_num	= str_replace('-', '', $end_date);
		if($start_date_num > 0) {
			add_post_meta( $post->ID, 'start_date_num', $start_date_num, true ) || update_post_meta( $post->ID, 'start_date_num', $start_date_num );
		}
		if($end_date_num > 0) {
			add_post_meta( $post->ID, 'end_date_num', $end_date_num, true ) || update_post_meta( $post->ID, 'end_date_num', $end_date_num );
		}		
	$i++; 
	endwhile; 
	endif; 
	rewind_posts(); 
	wp_reset_query();	
}
//add_action( 'wp_footer', 'save_num_date' );

function order_by_start_date( $query ) {
	if( $query->is_main_query() && !is_admin() ) {
		if(is_post_type_archive('activities')) {
			if($_GET['date_filter'] > 0) {
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'meta_key', 'start_date_num' );
				$query->set( 'order', 'DESC' );
				if($_GET['date_filter'] > 0) {
					$arr = array();		
					$current_date = date('ndY');
					switch ($_GET['date_filter']) {
						case '1':
							$arr['key'] = 'start_date_num';
							$arr['value'] = $current_date;
							$arr['compare'] = '>';
							$arr['type'] = 'NUMERIC';	
							$query->set( 'meta_query', $arr );					
							break;
						case '2':
							$arr_1 = array();
							$arr_1['key'] = 'start_date_num';
							$arr_1['value'] = $current_date;
							$arr_1['compare'] = '<=';
							$arr_1['type'] = 'NUMERIC';	

							$arr_2 = array();
							$arr_2['key'] = 'end_date_num';
							$arr_2['value'] = $current_date;
							$arr_2['compare'] = '>=';
							$arr_2['type'] = 'NUMERIC';

							$arr['relation'] = 'AND';
							$arr[] = $arr_1;
							$arr[] = $arr_2;
							$query->set( 'meta_query', $arr );					
							break;	
						case '3':
							$arr['key'] = 'end_date_num';
							$arr['value'] = $current_date;
							$arr['compare'] = '<';
							$arr['type'] = 'NUMERIC';	
							$query->set( 'meta_query', $arr );					
							break;											
					}
					$query->set( 'orderby', 'meta_value_num' );
					$query->set( 'meta_key', 'order_status' );
					$query->set( 'order', 'DESC' );	
					if(3 == $_GET['date_filter']) {
						$query->set( 'order', 'ASC' );	
					}				
				}				
			} else {
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'meta_key', 'order_status' );
				$query->set( 'order', 'DESC' );
			}
		}
	}
    return $query;
}
add_filter( 'pre_get_posts', 'order_by_start_date' );

function update_activity_order_status($k = 0) {
	if(!get_option('activity_updated') || get_option('activity_updated')!= date('ndY') || 1 == $k) {
		$i = 1;
		$query = new WP_Query();
		$args = array(
		    'post_type' => 'activities',
		    'showposts' => '-1'		
			);
		if($post_id > 0) {
			$args['p'] = $post_id;
		}
		$query->query($args);  
		if($query->have_posts()) : while($query->have_posts()): $query->the_post(); 
		global $post;
		$current_date = date('ndY');
		$start_date_num = get_post_meta( get_the_ID(), 'start_date_num', true );
		$end_date_num = get_post_meta( get_the_ID(), 'end_date_num', true );
		if($start_date_num > $current_date) {
			$type = 20000000 + (1000000 - $start_date_num);
			$text = 'Coming soon';
		} elseif(($start_date_num <= $current_date) && ($end_date_num >= $current_date)) {
			$type = 30000000 + (1000000 - $start_date_num);
			$text = 'Ongoing';
		} elseif ($end_date_num < $current_date) {
			$type = 10000000 + (1000000 - $start_date_num);
			$text = 'Expired';
		}		
		add_post_meta( $post->ID, 'order_status', $type, true ) || update_post_meta( $post->ID, 'order_status', $type );
		$i++; 
		endwhile; 
		endif; 
		rewind_posts(); 
		wp_reset_query();
		update_option('activity_updated', date('ndY'));	
	}
}
add_action( 'wp_footer', 'update_activity_order_status' );