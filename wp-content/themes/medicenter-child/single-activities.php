<?php
/*
Template Name: Single post
*/
get_header();
setPostViews(get_the_ID());
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<h1 class="page_title" title="<?php the_title(); ?>"><?php the_title(); ?></h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php the_title(); ?>
					</li>
				</ul>
			</div>
			<?php
			/*get page with single post template set*/
			$post_template_page_array = get_pages(array(
				'post_type' => 'page',
				'post_status' => 'publish',
				//'number' => 1,
				'meta_key' => '_wp_page_template',
				'meta_value' => 'single.php'
			));
			$post_template_page = $post_template_page_array[0];
			$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name)):
			?>
			<div class="page_header_right">
				<?php
				dynamic_sidebar($sidebar->post_name);
				?>
			</div>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix">


			<?php
			if(count($post_template_page_array) && isset($post_template_page))
			{
				//echo $post_template_page->post_content . 'asd';
				$post_content = $post_template_page->post_content;
				$post_content = str_replace( 'sidebar-blog', 'activities_s_sidebar', $post_content);
				$post_content = str_replace( '[vc_widget_sidebar sidebar_id="activities_s_sidebar"', ' [bm_list] [vc_widget_sidebar sidebar_id="activities_s_sidebar"', $post_content);
				//echo wpb_js_remove_wpautop(apply_filters('the_content', $post_content));
				echo wpb_js_remove_wpautop(apply_filters('the_content', $post_content));
				global $post;
				$post = $post_template_page;
				setup_postdata($post);
			}
			else
				echo wpb_js_remove_wpautop(apply_filters('the_content', '[vc_row top_margin="none" el_position="first last"] [vc_column top_margin="none" width="2/3"] [single_post featured_image_size="default" columns="1" show_post_categories="1" show_post_author="1" comments="1" lightbox_icon_color="blue_light" el_position="first last"] [/vc_column] [vc_column top_margin="none" width="1/3"] [vc_widget_sidebar sidebar_id="activities_s_sidebar" top_margin="page_margin_top" el_position="first"] [box_header title="Photostream" type="h3" bottom_border="1" top_margin="page_margin_top_section"] [photostream id="photostream" images="21,15,16,17,18,19" featured_image_size="default" lightbox_icon_color="blue_light" images_loop="1" top_margin="none"] [vc_widget_sidebar sidebar_id="sidebar-blog-2" top_margin="page_margin_top_section" el_position="last"] [/vc_column] [/vc_row]'));
			?>

<?php
if( 0 ) {
?>

<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1425795524649{border-top-width: 1px !important;border-right-width: 1px !important;border-bottom-width: 1px !important;border-left-width: 1px !important;padding-top: 0px !important;padding-right: 12px !important;padding-bottom: 0px !important;padding-left: 12px !important;border-left-color: #777777 !important;border-left-style: solid !important;border-right-color: #777777 !important;border-right-style: solid !important;border-top-color: #777777 !important;border-top-style: solid !important;border-bottom-color: #777777 !important;border-bottom-style: solid !important;}</style>	

<div class="vc_row wpb_row vc_row-fluid sa_wrapper">
    <div class="vc_col-sm-8 wpb_column vc_column_container ">
        <div class="wpb_wrapper">
            <div class="vc_empty_space" style="height: 18px">
            	<span class="vc_empty_space_inner"></span>
            </div>

<?php
wp_reset_query();
?>

<?php
if( 0 ) {
?>
            <div class="wpb_text_column wpb_content_element  vc_custom_1425795524649">
                <div class="wpb_wrapper">
					<?php
						global $post;
						$disclaimer = get_post_meta( $post->ID, 'disclaimer', true );
						echo wpautop( $disclaimer );
					?>                	
                </div>
            </div>
<?php
}
?>

			<?php
				//get_template_part( 'cp_gallery_box' );	
			?>


            <div class="clearfix tabs page_margin_top" data-interval="0" style="margin-top:20px;">
                <ul class="tabs_navigation clearfix">
					<?php
						global $post;
						$zh_list = get_post_meta( $post->ID, 'zh_list', true );
						if( $zh_list > 0 ) {
						?>
	                    <li>
	                    	<a href="#1424123424-1-48">中文</a>
	                    </li>						
						<?php	
						}	
					?> 
					<?php
						global $post;
						$en_list = get_post_meta( $post->ID, 'en_list', true );
						if( $en_list > 0 ) {
						?>
	                    <li>
	                    	<a href="#1424163358083-2-5">English</a>
	                    </li>					
						<?php	
						}	
					?> 	
                </ul>

                <div id="1424123424-1-48" class="wpb_tab">

<?php
if( 0 ) {
?>
                    <div class="vc_empty_space" style="height: 18px">
                    	<span class="vc_empty_space_inner"></span>
                    </div>

                    <div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_center">
                        <div class="wpb_wrapper">
                            <?php echo get_the_post_thumbnail( get_the_ID(), 'grid_thumb' ); ?>
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
                        <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                        <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
<?php
}
?>





					<?php
						global $post;
						$zh_list = get_post_meta( $post->ID, 'zh_list', true );
						if( $zh_list > 0 ) {
							for ($i=0; $i < $zh_list; $i++) { 
								$title = get_post_meta( $post->ID, 'zh_list_' . $i . '_title', true );
								$img = get_post_meta( $post->ID, 'zh_list_' . $i . '_img', true );
								$desc = get_post_meta( $post->ID, 'zh_list_' . $i . '_desc', true );
								?>
				                    <div class="vc_empty_space" style="height: 18px">
				                    	<span class="vc_empty_space_inner"></span>
				                    </div>	
				                    <?php
				                    if( $title ) {
				                    	?>
				                    	<h3 class="box_header animation-slide"><?php echo $title; ?></h3>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $img ) {
				                    	?>
						                    <div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_center">
						                        <div class="wpb_wrapper">
						                            <img src="<?php echo wp_get_attachment_url( $img ); ?>" class=" vc_box_border_grey attachment-full" alt="" />
						                        </div>
						                    </div>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $desc ) {
				                    	?>
					                    <div class="wpb_text_column wpb_content_element ">
					                        <div class="wpb_wrapper">
					                            <?php echo wpautop( do_shortcode( $desc ) ); ?>
					                        </div>
					                    </div>
				                    	<?php
				                    }
				                    ?>		
				                    <div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
				                        <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
				                        <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
				                    </div>			
								<?php
							}	
						}	
					?>



                </div>
                <div id="1424163358083-2-5" class="wpb_tab">
            <div class="vc_empty_space" style="height: 18px">
            	<span class="vc_empty_space_inner"></span>
            </div>

					<?php
						global $post;
						$en_list = get_post_meta( $post->ID, 'en_list', true );
						if( $en_list > 0 ) {
							for ($i=0; $i < $en_list; $i++) { 
								$title = get_post_meta( $post->ID, 'en_list_' . $i . '_title', true );
								$img = get_post_meta( $post->ID, 'en_list_' . $i . '_img', true );
								$desc = get_post_meta( $post->ID, 'en_list_' . $i . '_desc', true );
								?>
				                    <?php
				                    if( $title ) {
				                    	?>
				                    	<h3 class="box_header animation-slide"><?php echo $title; ?></h3>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $img ) {
				                    	?>
						                    <div class="wpb_text_column wpb_content_element">
						                        <div class="wpb_wrapper">
						                            <img src="<?php echo wp_get_attachment_url( $img ); ?>" class=" vc_box_border_grey attachment-full" alt="" />
						                        </div>
						                    </div>
				                    	<?php
				                    }
				                    ?>	
				                    <?php
				                    if( $desc ) {
				                    	?>
					                    <div class="wpb_text_column wpb_content_element ">
					                        <div class="wpb_wrapper">
					                            <?php echo wpautop( do_shortcode( $desc ) ); ?>
					                        </div>
					                    </div>
				                    	<?php
				                    }
				                    ?>				
								<?php
							}	
						}	
					?>

                </div>
            </div>
        </div>
    </div>
</div>






		</div>
	</div>
</div>

<div class="single_slider">	
	<div class="flexslider">
		<ul class="slides">
			<?php
				wp_reset_query();
				global $post;
				$slides = get_post_meta( $post->ID, 'slides', true );
				if( $slides > 0 ) {
					for ($i=0; $i < $slides; $i++) { 
						$img = get_post_meta( $post->ID, 'slides_' . $i . '_img', true );
						$desc = get_post_meta( $post->ID, 'slides_' . $i . '_desc', true );
						if( $img ) {
						?>
			            <li>
							<div class="table">
								<div class="td">		
									<div class="img_box">		            	
						            	<img src="<?php echo wp_get_attachment_url( $img ); ?>" alt="" />
						            	<div class="desc">
						            		<div class="desc_inner">
						            		<?php echo wpautop( $desc ); ?>
						            		</div>
						            	</div>
						            	<a href="#" class="single_slider_close"></a>
											<div class="slider_pagination">
												<div class="inner"><span class="current"></span> / <span class="total"></span></div>
											</div>						            	
									</div>	
								</div>	
							</div>						            	
			            </li>									
						<?php
						}
					}	
				}	
			?>				
		</ul>
	</div>	
</div>

<link rel='stylesheet' id='js_composer_front-css'  href='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/css/js_composer.css?ver=4.3.5' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_custom_css-css'  href='<?php bloginfo( 'url' ); ?>/wp-content/uploads/js_composer/custom.css?ver=4.3.5' type='text/css' media='screen' />
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/js/js_composer_front.js?ver=4.3.5'></script>
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/lib/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js?ver=4.3.5'></script>
<script type='text/javascript' src='<?php bloginfo( 'url' ); ?>/wp-content/plugins/js_composer/assets/lib/jquery-waypoints/waypoints.min.js?ver=4.3.5'></script>  

<?php
}
?>

		</div>
	</div>
</div>

<style type="text/css">
.theme_page .post {
	float: none;
}
.comment_box,
.post_footer {
	display: none;
}
</style>

<?php
get_footer();
?>