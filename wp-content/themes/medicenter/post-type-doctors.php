<?php
//custom post type - doctors
function theme_doctors_init()
{


	$labels = array(
		'name' => _x('Doctors', 'post type general name', 'medicenter'),
		'singular_name' => _x('Doctor', 'post type singular name', 'medicenter'),
		'add_new' => _x('Add New', 'doctors', 'medicenter'),
		'add_new_item' => __('Add New Doctor', 'medicenter'),
		'edit_item' => __('Edit Doctor', 'medicenter'),
		'new_item' => __('New Doctor', 'medicenter'),
		'all_items' => __('All Doctors', 'medicenter'),
		'view_item' => __('View Doctor', 'medicenter'),
		'search_items' => __('Search Doctor', 'medicenter'),
		'not_found' =>  __('No doctors found', 'medicenter'),
		'not_found_in_trash' => __('No doctors found in Trash', 'medicenter'), 
		'parent_item_colon' => '',
		'menu_name' => __("Doctors", 'medicenter')
	);
	$args = array(  
		"labels" => $labels, 
		"public" => true,  
		"show_ui" => true,  
		"capability_type" => "post",  
		"menu_position" => 20,
		"hierarchical" => false,  
		"rewrite" => true,
		"supports" => array("title", "editor", "excerpt", "thumbnail", "page-attributes")  
	);
	register_post_type("doctors", $args);
	register_taxonomy("doctors_category", array("doctors"), array("label" => "Categories", "singular_label" => "Category", "rewrite" => true));
}  
add_action("init", "theme_doctors_init"); 
if ( is_admin() ) {
    function add_post_enctype() {
        echo "<script type='text/javascript'>
                  jQuery(document).ready(function(){
                      jQuery('#post').attr('enctype','multipart/form-data');
                  });
              </script>";
    }
    add_action('admin_head', 'add_post_enctype');
}
//Adds a box to the right column and to the main column on the Doctors edit screens
function theme_add_doctors_custom_box() 
{
	add_meta_box( 
        "doctors_config",
        __("Options", 'medicenter'),
        "theme_inner_doctors_custom_box_main",
        "doctors",
		"normal",
		"high"
    );
}
add_action("add_meta_boxes", "theme_add_doctors_custom_box");
//backwards compatible (before WP 3.0)
//add_action("admin_init", "theme_add_custom_box", 1);

function theme_inner_doctors_custom_box_main($post)
{
	?>

<style>
	#adduser .form-field input, input.regular-text{width:20em !important;}
	.inside button {font-size: 12px;}
	.interests { margin: 27px 0 0!important;}
</style>
<?php
	global $themename;
	//Use nonce for verification
	wp_nonce_field(plugin_basename( __FILE__ ), $themename . "_doctors_noncename");
	
	//The actual fields for data entry
	$timetable_page = get_post_meta($post->ID, "timetable_page", true);
	$icon_type = get_post_meta($post->ID, "social_icon_type", true);
	$icon_url = get_post_meta($post->ID, "social_icon_url", true);
	$icon_target = get_post_meta($post->ID, "social_icon_target", true);
	$icon_color = get_post_meta($post->ID, "social_icon_color", true);
	$attachment_ids = get_post_meta($post->ID, $themename. "_attachment_ids", true);
	$images = get_post_meta($post->ID, $themename. "_images", true);
	$images_titles = get_post_meta($post->ID, $themename. "_images_titles", true);
	$videos = get_post_meta($post->ID, $themename. "_videos", true);
	$iframes = get_post_meta($post->ID, $themename. "_iframes", true);
	$external_urls = get_post_meta($post->ID, $themename. "_external_urls", true);
	$features_images_loop = get_post_meta($post->ID, $themename. "_features_images_loop", true);
	
	$icons = array(
			"blogger",
			"deviantart",
			"dribbble",
			"envato",
			"facebook",
			"flickr",
			"form",
			"forrst",
			"googleplus",
			"instagram",
			"linkedin",
			"mail",
			"myspace",
			"phone",
			"picasa",
			"pinterest",
			"rss",
			"skype",
			"soundcloud",
			"stumbleupon",
			"tumblr",
			"twitter",
			"vimeo",
			"xing",
			"youtube"
		);
	
	echo '
	<table width="80%">
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('DOB', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="DOB" value="' . esc_attr(get_post_meta($post->ID, "DOB", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('DD/MM/YYYY ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Gender', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Gender" value="' . esc_attr(get_post_meta($post->ID, "Gender", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('M; F; ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Nationality', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Nationality" value="' . esc_attr(get_post_meta($post->ID, "Nationality", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('Country; ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Spoken Languages', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Spoken_Languages" value="' . esc_attr(get_post_meta($post->ID, "Spoken_Languages", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('separate by / ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Postal address', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Postal_address1" value="' . esc_attr(get_post_meta($post->ID, "Postal_address1", true)) . '" />
				<input class="regular-text" type="text" id="doctor_subtitle" name="Postal_address2" value="' . esc_attr(get_post_meta($post->ID, "Postal_address2", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('Line1,Line2 ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Email address', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="email" value="' . esc_attr(get_post_meta($post->ID, "email", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('Private email address preferred ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr colspan="3"></tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Key Title 1', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Key_Title_1" value="' . esc_attr(get_post_meta($post->ID, "Key_Title_1", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Organisation', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Organisation1" value="' . esc_attr(get_post_meta($post->ID, "Organisation1", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Key Title 2', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Key_Title_2" value="' . esc_attr(get_post_meta($post->ID, "Key_Title_2", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Organisation', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Organisation2" value="' . esc_attr(get_post_meta($post->ID, "Organisation2", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr><tr>
			<td>
				<label for="doctor_subtitle">' . __('Key Title 3', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Key_Title_3" value="' . esc_attr(get_post_meta($post->ID, "Key_Title_3", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Organisation', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Organisation3" value="' . esc_attr(get_post_meta($post->ID, "Organisation3", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Professor ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Educational Degree', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Educational_Degree" value="' . esc_attr(get_post_meta($post->ID, "Educational_Degree", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. PhD ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Base location', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Base_location" value="' . esc_attr(get_post_meta($post->ID, "Base_location", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('City, Country  ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('international M.D. License ', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="international_License" value="' . esc_attr(get_post_meta($post->ID, "international_License", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('Countries, separate by /  ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Specialty', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Specialty" value="' . esc_attr(get_post_meta($post->ID, "Specialty", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Neurosurgery ', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Subspecialty', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Subspecialty" value="' . esc_attr(get_post_meta($post->ID, "Subspecialty", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('e.g. Vascular, separate by /', 'medicenter') . '</span>
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="doctor_subtitle">' . __('Clinical interest', 'medicenter') . ':</label></td>
			<td>
				<table width="80%" id="clinical">
					<tr>
						<td>Disease or area name</td>
						<td>how many cases done?</td>
					</tr>';
					$totalclinical= esc_attr(get_post_meta($post->ID, "totalclinical", true));
					if($totalclinical==0){
						$totalclinical=1;
					}
					for($x=0;$x<$totalclinical;$x++){
					echo '
					<tr>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="clinical'.$x.'Disease" value="' . esc_attr(get_post_meta($post->ID, "clinical".$x."Disease", true)) . '" /></td>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="clinical'.$x.'cases" value="' . esc_attr(get_post_meta($post->ID, "clinical".$x."cases", true)) . '" /></td>
					</tr>';
				}	
				echo '
				</table>
			</td>
			<td valign="">
				<button onclick="add_clinical();" type="button" class="interests button">Add Clinical Interest</button>
				<input id="clinical_field" type="hidden" name="totalclinical" value="' . $totalclinical . '" />
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="doctor_subtitle">' . __('Surgical interest', 'medicenter') . ':</label></td>
			<td>
				<table width="100%" id="surgical">
					<tr>
						<td>Disease or area name</td>
						<td>how many cases done?</td>
					</tr>';
					$totalclinical= esc_attr(get_post_meta($post->ID, "totalsurgical", true));
					if($totalclinical==0){
						$totalclinical=1;
					}
					for($x=0;$x<$totalclinical;$x++){
					echo '
					<tr>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="surgical'.$x.'Disease" value="' . esc_attr(get_post_meta($post->ID, "surgical".$x."Disease", true)) . '" /></td>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="surgical'.$x.'cases" value="' . esc_attr(get_post_meta($post->ID, "surgical".$x."cases", true)) . '" /></td>
					</tr>';
				}
				echo '
				</table>
			</td>
			<td valign="">
				<button onclick="add_surgical();" type="button" class="interests button">Add Surgical Interest</button>
				<input id="surgical_field" type="hidden"  name="totalsurgical" value="' . $totalclinical . '" />
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="doctor_subtitle">' . __('Research interest', 'medicenter') . ':</label></td>
			<td>
				<table width="100%" id="research">
					<tr>
						<td>Disease or area name</td>
						<td>how many cases done?</td>
					</tr>';
					$totalclinical= esc_attr(get_post_meta($post->ID, "totalresearch", true));
					if($totalclinical==0){
						$totalclinical=1;
					}
					for($x=0;$x<$totalclinical;$x++){
					echo '
					<tr>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="research'.$x.'Disease" value="' . esc_attr(get_post_meta($post->ID, "research".$x."Disease", true)) . '" /></td>
						<td><input class="regular-text" type="text" id="doctor_subtitle" name="research'.$x.'cases" value="' . esc_attr(get_post_meta($post->ID, "research".$x."cases", true)) . '" /></td>
					</tr>';
				}
				echo '
				</table>
			</td>
			<td valign="">
			<button onclick="add_research();" type="button" class="interests button">Add Surgical Interest</button>
			<input id="research_field"  name="totalresearch" value="' . $totalclinical . '"  type="hidden"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="doctor_subtitle">' . __('Biography', 'medicenter') . ':</label>
			</td>
			<td>
				<input class="regular-text" type="text" id="doctor_subtitle" name="Biography" value="' . esc_attr(get_post_meta($post->ID, "Biography", true)) . '" />
			</td>
			<td>
				<span class="description">' . __('In third person, please write about yourself and your achievement: 
e.g. numbers of chapters written; numbers of thesis written; numbers of books written;  numbers of cases treated. ', 'medicenter') . '</span>
			</td>
		</tr>
	</table>
	';?>
	<script>
function add_clinical() {
	var val=jQuery("#clinical_field").val();
	console.log(val);
	var row = '<tr><td><input class="regular-text" type="text" id="doctor_subtitle" name="clinical'+val+'Disease" value="" /></td><td><input class="regular-text" type="text" id="doctor_subtitle" name="clinical'+val+'cases" value="" /></td></tr>';
	val++;
	jQuery('#clinical').append(row);
	jQuery("#clinical_field").val(val);
}
function add_surgical() {
	var val=jQuery("#surgical_field").val();
	console.log(val);
	var row = '<tr><td><input class="regular-text" type="text" id="doctor_subtitle" name="surgical'+val+'Disease" value="" /></td><td><input class="regular-text" type="text" id="doctor_subtitle" name="surgical'+val+'cases" value="" /></td></tr>';
	jQuery('#surgical').append(row);
	val++;
	jQuery("#surgical_field").val(val);
}
function add_research() {
	var val=jQuery("#research_field").val();
	console.log(val);
	var row = '<tr><td><input class="regular-text" type="text" id="doctor_subtitle" name="research'+val+'Disease" value="" /></td><td><input class="regular-text" type="text" id="doctor_subtitle" name="research'+val+'cases" value="" /></td></tr>';
	val++;
	jQuery('#research').append(row);
	jQuery("#research_field").val(val);
}
function add_update_field() {
	var val=jQuery("#update_field").val();
	console.log(val);
	var row = '<tr><td><input type="hidden" value="" id="medicenter_attachment_id_0" name="update0attachment_id"><input type="text" name="update'+val+'image" id="medicenter_image_url_0" class="regular-text"><input type="button" value="Browse" id="medicenter_image_url_button_0" name="medicenter_upload_button" class="button"></td><td>Title: <input class="regular-text" type="text" id="doctor_subtitle" name="update'+val+'" value="" /></td><td><table width="100%" id="update'+val+'"><tbody><tr><td>Description:  </td></tr><tr><td>Text: <textarea name="update'+val+'description0" id="doctor_subtitle" class="regular-text"></textarea></td><td>image: <input type="hidden" value="" id="medicenter_attachment_id_'+val+'" name="update2attachment_id"><input type="text" value="" name="update'+val+'description_image0" id="medicenter_image_url_2" class="regular-text"><input type="button" value="Browse" id="medicenter_image_url_button_'+val+'" name="medicenter_upload_button" class="button"></td></td></tr></tbody></table></td><td><button type="button" onclick="add_update_description('+val+');">Add More description</button><input type="hidden" value="1" name="totalupdatedescription'+val+'" id="update_field_description'+val+'"></td></tr>';
	val++;
	jQuery('#update').append(row);
	jQuery("#update_field").val(val);
}
function add_update_description(id) {
	console.log(id);
	var val=jQuery("#update_field_description"+id).val();
	console.log(val);
	var row = '<tr><td>Text: <textarea name="update'+id+'description'+val+'" id="doctor_subtitle" class="regular-text"></textarea></td><td>image: <input type="hidden" value="" id="medicenter_attachment_id_0" name="update'+id+'attachment_id"><input type="text" value="" name="update'+id+'description_image'+val+'" id="medicenter_image_url_0" class="regular-text"><input type="button" value="Browse" id="medicenter_image_url_button_0" name="medicenter_upload_button" class="button"></td></tr>';
	val++;
	jQuery('#update'+id).append(row);
	jQuery("#update_field_description"+id).val(val);
}
</script>
	<?php
}

//When the post is saved, saves our custom data
function theme_save_doctors_postdata($post_id) 
{
	global $themename;
	//verify if this is an auto save routine. 
	//if it is our form has not been submitted, so we dont want to do anything
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
		return;

	//verify this came from the our screen and with proper authorization,
	//because save_post can be triggered at other times
	if (!wp_verify_nonce($_POST[$themename . '_doctors_noncename'], plugin_basename( __FILE__ )))
		return;


	//Check permissions
	if(!current_user_can('edit_post', $post_id))
		return;

	//OK, we're authenticated: we need to find and save the data
	/*****Theme update****/
	
	//print_r($_POST['totalclinical']);
	
	update_post_meta($post_id, "totalclinical", $_POST["totalclinical"]);
	for($x=0;$x<$_POST['totalclinical'];$x++){
		update_post_meta($post_id, "clinical".$x."Disease", $_POST["clinical".$x."Disease"]);
		update_post_meta($post_id, "clinical".$x."cases", $_POST["clinical".$x."cases"]);
	}
	update_post_meta($post_id, "totalresearch", $_POST["totalresearch"]);
	for($x=0;$x<$_POST['totalresearch'];$x++){
		update_post_meta($post_id, "research".$x."Disease", $_POST["research".$x."Disease"]);
		update_post_meta($post_id, "research".$x."cases", $_POST["research".$x."cases"]);
	}
	update_post_meta($post_id, "totalsurgical", $_POST["totalsurgical"]);
	for($x=0;$x<$_POST['totalsurgical'];$x++){
		update_post_meta($post_id, "surgical".$x."Disease", $_POST["surgical".$x."Disease"]);
		update_post_meta($post_id, "surgical".$x."cases", $_POST["surgical".$x."cases"]);
	}
	update_post_meta($post_id, "totalupdate", $_POST["totalupdate"]);
	for($x=0;$x<$_POST['totalupdate'];$x++){
		update_post_meta($post_id, "update".$x."", $_POST["update".$x.""]);
		update_post_meta($post_id, "update".$x."image", $_POST["update".$x."image"]);
		update_post_meta($post_id, "totalupdatedescription".$x."", $_POST["totalupdatedescription".$x.""]);
		for($y=0;$y<$_POST['totalupdatedescription'.$x.''];$y++){
		update_post_meta($post_id, "update".$x."description".$y."", $_POST["update".$x."description".$y.""]);
		update_post_meta($post_id, "update".$x."description_image".$y."", $_POST["update".$x."description_image".$y.""]);
		}
		/* $themeta = get_post_meta($post_id, "update".$x."", TRUE);
		echo $themeta;
		if($themeta != '') {
			
			update_post_meta($post_id, "update".$x."", $_POST["update".$x.""]);
			update_post_meta($post_id, "update".$x."image", $_POST["update".$x."image"]);
		}else{
			
			add_post_meta($post_id, "update".$x."", $_POST["update".$x.""]);
			add_post_meta($post_id, "update".$x."image", $_POST["update".$x."image"]);
		} */
	}
	//print_r($_POST);die;
	//die;
	/* print_r($_POST['totalclinical']);
	print_r($_POST['totalresearch']);
	print_r($_POST['totalsurgical']); */
	//print_r($_POST);
	//die;
	/* die;
	$updates=json_encode($_POST['updates'], JSON_FORCE_OBJECT);
	$clinical=json_encode($_POST['clinical'] , JSON_FORCE_OBJECT);
	$surgical=json_encode($_POST['surgical'] , JSON_FORCE_OBJECT);
	$research=json_encode($_POST['research'] , JSON_FORCE_OBJECT); */
	update_post_meta($post_id, "clinical", $clinical);
	update_post_meta($post_id, "updates", $updates);
	update_post_meta($post_id, "surgical", $surgical);
	update_post_meta($post_id, "research", $research);
	update_post_meta($post_id, "DOB", $_POST["DOB"]);
	update_post_meta($post_id, "Gender", $_POST["Gender"]);
	update_post_meta($post_id, "Nationality", $_POST["Nationality"]);
	update_post_meta($post_id, "Spoken_Languages", $_POST["Spoken_Languages"]);
	update_post_meta($post_id, "Postal_address1", $_POST["Postal_address1"]);
	update_post_meta($post_id, "Postal_address2", $_POST["Postal_address2"]);
	update_post_meta($post_id, "email", $_POST["email"]);
	update_post_meta($post_id, "Key_Title_1", $_POST["Key_Title_1"]);
	update_post_meta($post_id, "Organisation1", $_POST["Organisation1"]);
	update_post_meta($post_id, "Key_Title_2", $_POST["Key_Title_2"]);
	update_post_meta($post_id, "Organisation2", $_POST["Organisation2"]);
	update_post_meta($post_id, "Key_Title_3", $_POST["Key_Title_3"]);
	update_post_meta($post_id, "Organisation3", $_POST["Organisation3"]);
	update_post_meta($post_id, "Educational_Degree", $_POST["Educational_Degree"]);
	update_post_meta($post_id, "Base_location", $_POST["Base_location"]);
	update_post_meta($post_id, "international_License", $_POST["international_License"]);
	update_post_meta($post_id, "Specialty", $_POST["Specialty"]);
	update_post_meta($post_id, "Subspecialty", $_POST["Subspecialty"]);
	update_post_meta($post_id, "Biography", $_POST["Biography"]);
	/*****Theme update****/
	update_post_meta($post_id, "subtitle", $_POST["doctor_subtitle"]);
	update_post_meta($post_id, "image_title", $_POST["image_title"]);
	update_post_meta($post_id, "video_url", $_POST["doctors_video_url"]);
	update_post_meta($post_id, "iframe_url", $_POST["doctors_iframe_url"]);
	update_post_meta($post_id, "external_url", $_POST["doctors_external_url"]);
	update_post_meta($post_id, "external_url_target", $_POST["doctors_external_url_target"]);
	update_post_meta($post_id, "timetable_page", $_POST["doctor_timetable_page"]);
	$icon_type = (array)$_POST["icon_type"];
	while(end($icon_type)==="")
		array_pop($icon_type);
	update_post_meta($post_id, "social_icon_type", $icon_type);
	update_post_meta($post_id, "social_icon_url", $_POST["icon_url"]);
	update_post_meta($post_id, "social_icon_target", $_POST["icon_target"]);
	update_post_meta($post_id, "social_icon_color", $_POST["icon_color"]);
	update_post_meta($post_id, $themename . "_attachment_ids", $_POST["attachment_ids"]);
	$images = (array)$_POST["images"];
	while(end($images)==="")
		array_pop($images);
	update_post_meta($post_id, $themename . "_images", $images);
	update_post_meta($post_id, $themename . "_images_titles", $_POST["images_titles"]);
	update_post_meta($post_id, $themename . "_videos", $_POST["videos"]);
	update_post_meta($post_id, $themename . "_iframes", $_POST["iframes"]);
	update_post_meta($post_id, $themename . "_external_urls", $_POST["external_urls"]);
	update_post_meta($post_id, $themename . "_features_images_loop", $_POST["features_images_loop"]);
}
add_action("save_post", "theme_save_doctors_postdata");

function doctors_edit_columns($columns)
{
	$columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"title" => _x('Title', 'post type singular name', 'medicenter'),
			"doctors_category" => __('Categories', 'medicenter'),
			"date" => __('Date', 'medicenter')
	);

	return $columns;
}
add_filter("manage_edit-doctors_columns", "doctors_edit_columns");

function manage_doctors_posts_custom_column($column)
{
	global $post;
	switch ($column)
	{
		case "doctors_category":
			echo get_the_term_list($post->ID, "doctors_category", '', ', ','');
			break;
	}
}
add_action("manage_doctors_posts_custom_column", "manage_doctors_posts_custom_column");

add_shortcode("doctors", "theme_gallery_shortcode");
//ajax pagination
add_action("wp_ajax_theme_doctors_pagination", "theme_gallery_shortcode");
add_action("wp_ajax_nopriv_theme_doctors_pagination", "theme_gallery_shortcode");

//visual composer
function theme_doctors_vc_init()
{
	//get doctors list
	$doctors_list = get_posts(array(
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'post_type' => 'doctors'
	));
	$doctors_array = array();
	$doctors_array[__("All", 'medicenter')] = "-";
	foreach($doctors_list as $doctor)
		$doctors_array[$doctor->post_title . " (id:" . $doctor->ID . ")"] = $doctor->ID;

	//get doctors categories list
	$doctors_categories = get_terms("doctors_category");
	$doctors_categories_array = array();
	$doctors_categories_array[__("All", 'medicenter')] = "-";
	foreach($doctors_categories as $doctors_category)
		$doctors_categories_array[$doctors_category->name] =  $doctors_category->slug;
	
	//get all pages
	$pages_array = array();
	$pages_array["-"] = "-";
	$args = array(
		'post_type' => 'page',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'orderby' => 'title', 
		'order' => 'ASC'
	);
	query_posts($args);
	if(have_posts()) : while (have_posts()) : the_post();
		$pages_array[get_the_title()] = get_the_ID();
	endwhile;
	endif;
	wp_reset_query();
	
	//image sizes
	$image_sizes_array = array();
	$image_sizes_array[__("Default", 'medicenter')] = "default";
	global $_wp_additional_image_sizes;
	foreach(get_intermediate_image_sizes() as $s) 
	{
		if(isset($_wp_additional_image_sizes[$s])) 
		{
			$width = intval($_wp_additional_image_sizes[$s]['width']);
			$height = intval($_wp_additional_image_sizes[$s]['height']);
		} 
		else
		{
			$width = get_option($s.'_size_w');
			$height = get_option($s.'_size_h');
		}
		$image_sizes_array[$s . " (" . $width . "x" . $height . ")"] = "mc_" . $s;
	}

	wpb_map( array(
		"name" => __("Doctors list", 'medicenter'),
		"base" => "doctors",
		"class" => "",
		"controls" => "full",
		"show_settings_on_create" => true,
		"icon" => "icon-wpb-layer-doctors-list",
		"category" => __('MediCenter', 'medicenter'),
		"params" => array(
			array(
				"type" => "hidden",
				"class" => "",
				"param_name" => "shortcode_type",
				"value" => "doctors"
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Header", 'medicenter'),
				"param_name" => "header",
				"value" => __('Meet The Team', 'medicenter')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Header border animation", 'medicenter'),
				"param_name" => "animation",
				"value" => array(__("no", 'medicenter') => 0,  __("yes", 'medicenter') => 1)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Order by", 'medicenter'),
				"param_name" => "order_by",
				"value" => array(__("Title, menu order", 'medicenter') => "title,menu_order", __("Menu order", 'medicenter') => "menu_order", __("Date", 'medicenter') => "date")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Order", 'medicenter'),
				"param_name" => "order",
				"value" => array(__("ascending", 'medicenter') => "ASC", __("descending", 'medicenter') => "DESC")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Type", 'medicenter'),
				"param_name" => "type",
				"value" => array(__("List with details", 'medicenter') => "list_with_details", __("List", 'medicenter') => "list", __("Details", 'medicenter') => "details")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Layout", 'medicenter'),
				"param_name" => "layout",
				"value" => array(__("4 columns", 'medicenter') => "gallery_4_columns", __("2 columns", 'medicenter') => "gallery_2_columns", __("3 columns", 'medicenter') => "gallery_3_columns")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Featured image size", 'medicenter'),
				"param_name" => "featured_image_size",
				"value" => $image_sizes_array
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Hover icons", 'medicenter'),
				"param_name" => "hover_icons",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => "0")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Title box in details", 'medicenter'),
				"param_name" => "title_box",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => 0),
				"dependency" => Array('element' => "type", 'value' => array('list_with_details'))
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Details page", 'medicenter'),
				"param_name" => "details_page",
				"value" => $pages_array,
				"dependency" => Array('element' => "type", 'value' => array('list'))
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Display method", 'medicenter'),
				"param_name" => "display_method",
				"value" => array(__("Filters", 'medicenter') => 'dm_filters', __("Carousel", 'medicenter') => 'dm_carousel', __("Pagination", 'medicenter') => 'dm_pagination', __("Simple", 'medicenter') => 'dm_simple'),
				"dependency" => Array('element' => "type", 'value' => array('list_with_details', 'list'))
			),
			//filters options
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("All filter label", 'medicenter'),
				"param_name" => "all_label",
				"value" => __('All Doctors', 'medicenter'),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_filters')
			),
			//carousel options
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Id", 'medicenter'),
				"param_name" => "id",
				"value" => "carousel",
				"description" => __("Please provide unique id for each carousel on the same page/post", 'medicenter'),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Autoplay", 'medicenter'),
				"param_name" => "autoplay",
				"value" => array(__("No", 'medicenter') => 0, __("Yes", 'medicenter') => 1),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Pause on hover", 'medicenter'),
				"param_name" => "pause_on_hover",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => 0),
				"description" => __("Affect only when autoplay is set to yes", 'medicenter'),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Scroll", 'medicenter'),
				"param_name" => "scroll",
				"value" => 1,
				"description" => __("Number of items to scroll in one step", 'medicenter'),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Effect", 'medicenter'),
				"param_name" => "effect",
				"value" => array(
					__("scroll", 'medicenter') => "scroll", 
					__("none", 'medicenter') => "none", 
					__("directscroll", 'medicenter') => "directscroll",
					__("fade", 'medicenter') => "_fade",
					__("crossfade", 'medicenter') => "crossfade",
					__("cover", 'medicenter') => "cover",
					__("uncover", 'medicenter') => "uncover"
				),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Sliding easing", 'medicenter'),
				"param_name" => "easing",
				"value" => array(
					__("swing", 'medicenter') => "swing", 
					__("linear", 'medicenter') => "linear", 
					__("easeInQuad", 'medicenter') => "easeInQuad",
					__("easeOutQuad", 'medicenter') => "easeOutQuad",
					__("easeInOutQuad", 'medicenter') => "easeInOutQuad",
					__("easeInCubic", 'medicenter') => "easeInCubic",
					__("easeOutCubic", 'medicenter') => "easeOutCubic",
					__("easeInOutCubic", 'medicenter') => "easeInOutCubic",
					__("easeInQuart", 'medicenter') => "easeInQuart",
					__("easeOutQuart", 'medicenter') => "easeOutQuart",
					__("easeInOutQuart", 'medicenter') => "easeInOutQuart",
					__("easeInSine", 'medicenter') => "easeInSine",
					__("easeOutSine", 'medicenter') => "easeOutSine",
					__("easeInOutSine", 'medicenter') => "easeInOutSine",
					__("easeInExpo", 'medicenter') => "easeInExpo",
					__("easeOutExpo", 'medicenter') => "easeOutExpo",
					__("easeInOutExpo", 'medicenter') => "easeInOutExpo",
					__("easeInQuint", 'medicenter') => "easeInQuint",
					__("easeOutQuint", 'medicenter') => "easeOutQuint",
					__("easeInOutQuint", 'medicenter') => "easeInOutQuint",
					__("easeInCirc", 'medicenter') => "easeInCirc",
					__("easeOutCirc", 'medicenter') => "easeOutCirc",
					__("easeInOutCirc", 'medicenter') => "easeInOutCirc",
					__("easeInElastic", 'medicenter') => "easeInElastic",
					__("easeOutElastic", 'medicenter') => "easeOutElastic",
					__("easeInOutElastic", 'medicenter') => "easeInOutElastic",
					__("easeInBack", 'medicenter') => "easeInBack",
					__("easeOutBack", 'medicenter') => "easeOutBack",
					__("easeInOutBack", 'medicenter') => "easeInOutBack",
					__("easeInBounce", 'medicenter') => "easeInBounce",
					__("easeOutBounce", 'medicenter') => "easeOutBounce",
					__("easeInOutBounce", 'medicenter') => "easeInOutBounce"
				),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Sliding transition speed (ms)", 'medicenter'),
				"param_name" => "duration",
				"value" => 500,
				"dependency" => Array('element' => "display_method", 'value' => 'dm_carousel')
			),
			//pagination options
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Items per page", 'medicenter'),
				"param_name" => "items_per_page",
				"value" => 4,
				"dependency" => Array('element' => "display_method", 'value' => 'dm_pagination')
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Ajax pagination", 'medicenter'),
				"param_name" => "ajax_pagination",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => 0),
				"dependency" => Array('element' => "display_method", 'value' => 'dm_pagination')
			),
			array(
				"type" => "dropdownmulti",
				"class" => "",
				"heading" => __("Display selected", 'medicenter'),
				"param_name" => "ids",
				"value" => $doctors_array
			),
			array(
				"type" => "dropdownmulti",
				"class" => "",
				"heading" => __("Display from Category", 'medicenter'),
				"param_name" => "category",
				"value" => $doctors_categories_array
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Display headers in details", 'medicenter'),
				"param_name" => "display_headers",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => 0),
				"dependency" => Array('element' => "type", 'value' => array('list_with_details', 'details'))
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Headers type", 'medicenter'),
				"param_name" => "headers_type",
				"value" => array(__("H2", 'medicenter') => "h2", __("H1", 'medicenter') => "h1", __("H3", 'medicenter') => "h3", __("H4", 'medicenter') => "h4", __("H5", 'medicenter') => "h5"),
				"dependency" => Array('element' => "type", 'value' => array('list_with_details', 'details'))
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Display social icons", 'medicenter'),
				"param_name" => "display_social_icons",
				"value" => array(__("Yes", 'medicenter') => 1, __("No", 'medicenter') => 0)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Lightbox images loop", 'medicenter'),
				"param_name" => "images_loop",
				"value" => array(__("No", 'medicenter') => 0, __("Yes", 'medicenter') => 1),
				"dependency" => Array('element' => "type", 'value' => array('list_with_details', 'list'))
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Lightbox icon color", 'medicenter'),
				"param_name" => "lightbox_icon_color",
				"value" => array(
					__("light blue", 'medicenter') => 'blue_light', 
					__("dark blue", 'medicenter') => 'blue_dark',
					__("blue", 'medicenter') => 'blue',
					__("black", 'medicenter') => 'black',
					__("gray", 'medicenter') => 'gray',
					__("dark gray", 'medicenter') => 'gray_dark',
					__("light gray", 'medicenter') => 'gray_light',
					__("green", 'medicenter') => 'green',
					__("dark green", 'medicenter') => 'green_dark',
					__("light green", 'medicenter') => 'green_light',
					__("orange", 'medicenter') => 'orange',
					__("dark orange", 'medicenter') => 'orange_dark',
					__("light orange", 'medicenter') => 'orange_light',
					__("red", 'medicenter') => 'red',
					__("dark red", 'medicenter') => 'red_dark',
					__("light red", 'medicenter') => 'red_light',
					__("turquoise", 'medicenter') => 'turquoise',
					__("dark turquoise", 'medicenter') => 'turquoise_dark',
					__("light turquoise", 'medicenter') => 'turquoise_light',
					__("violet", 'medicenter') => 'violet',
					__("dark violet", 'medicenter') => 'violet_dark',
					__("light violet", 'medicenter') => 'violet_light',
					__("white", 'medicenter') => 'white',
					__("yellow", 'medicenter') => 'yellow'
				)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Top margin", 'medicenter'),
				"param_name" => "top_margin",
				"value" => array(__("Section (large)", 'medicenter') => "page_margin_top_section", __("Page (small)", 'medicenter') => "page_margin_top", __("None", 'medicenter') => "none")
			)
		)
	));
}
add_action("init", "theme_doctors_vc_init");
?>
