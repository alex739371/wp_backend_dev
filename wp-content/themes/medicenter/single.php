<style>
.flag-cont {
  float: left;
  width: 30%;
}
.cntry-name {
  float: left;
  width: 100%;
  text-align:left;
}
.flag-cont .flag {
  height: 100px;
  width: 100%;
}
.wpb_wrapper {
    text-align: center;
}
	.vc_custom_1442469232786 {
		margin-top: 10px !important;
		margin-bottom: 10px !important;
		background-image: url(<?php echo get_home_url();?>/wp-content/uploads/2015/09/grunge-background-K.jpg) !important;
	}
	.vc_custom_1442467344544 {
		margin-top: 10px !important;
		margin-left: 10px !important;
		margin-bottom: 10px !important;
		border-top-width: 1px !important;
		border-right-width: 1px !important;
		border-bottom-width: 1px !important;
		border-left-width: 1px !important;
		background-color: #008000 !important;
	}
	.vc_custom_1442458537808 {
		margin-top: 16px !important;
		margin-left: 10px !important;
		background-color: #357ca8 !important;
	}
</style>
<?php

	get_header();
	setPostViews(get_the_ID());
	$country_codes = array(
		'AR'=>'ARGENTINA',
	'AU'=>'AUSTRALIA',
	'AT'=>'AUSTRIA',
	'BE'=>'BELGIUM',
	'CA'=>'CANADA',
	'CN'=>'CHINA',
	'FI'=>'FINLAND',
	'FR'=>'FRANCE',
	'DE'=>'GERMANY',
	'IN'=>'INDIA',
	'IT'=>'ITALY',
	'JP'=>'JAPAN',
	'PL'=>'POLAND',
	'PT'=>'PORTUGAL',
	'ES'=>'SPAIN',
	'SE'=>'SWEDEN',
	'CH'=>'SWITZERLAND',
	'AE'=>'UNITED ARAB EMIRATES',
	'GB'=>'UNITED KINGDOM',
	'US'=>'UNITED STATES',
	);
?>
<link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/flatly/bootstrap.min.css" rel="stylesheet" id="bootstrap">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_directory'); ?>/flags/assets/docs.css" rel="stylesheet">
<link href="<?php bloginfo('template_directory'); ?>/flags/css/flag-icon.css" rel="stylesheet">
<style>
	.vc_custom_1442469232786 {
		margin-top: 10px !important;
		margin-bottom: 10px !important;
		background-image: url(<?php echo get_home_url();?>/wp-content/uploads/2015/09/grunge-background-K.jpg) !important;
	}
	.vc_custom_1442467344544 {
		margin-top: 10px !important;
		margin-left: 10px !important;
		margin-bottom: 10px !important;
		border-top-width: 1px !important;
		border-right-width: 1px !important;
		border-bottom-width: 1px !important;
		border-left-width: 1px !important;
		background-color: #008000 !important;
	}
	.vc_custom_1442458537808 {
		margin-top: 16px !important;
		margin-left: 10px !important;
		background-color: #357ca8 !important;
	}
</style>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<h1 class="page_title"><?php the_title(); ?></h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'gymbase'); ?>">
							<?php _e('Home', 'gymbase'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_white">
						&nbsp;
					</li>
					<li>
						<?php the_title(); ?>
					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix">
			<div class="vc_row wpb_row vc_row-fluid">
				<?php if(have_posts()) : while (have_posts()) : the_post();	?>
					<?php 
						$post = get_post();$post_classes = get_post_class("post");
						$output = '';
//						echo $term_id = get_cat_ID('doctors_category');
						foreach($post_classes as $key=>$post_class)
								$output .= $key .' '. $post_class . ($key+1<count($post_classes) ? ' ' : '').'<br />';						
					?>
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_row-fluid ">
							<div class="vc_col-sm-12 wpb_column vc_column_container ">
								<div class="wpb_wrapper">
									<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
								</div> 
							</div> 
						</div>
						<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_appear vc_align_center">
							<div class="wpb_wrapper">
						<?php
							if(has_post_thumbnail()):
								$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "large");
								$large_image_url = $attachment_image[0];
							?>
							<a class="vc_box_border_grey attachment-full fancybox" href="<?php echo $large_image_url; ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail("large", array("alt" => get_the_title(), "title" => "")); ?>
							</a>
							<?php
							endif;
						?>
<!--					<h2>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="text">
							<?php the_content(); ?>
						</div>
						<div class="post_footer">
							<ul class="categories">
								<li class="posted_by"><?php _e('Posted by', 'gymbase'); echo " "; if(get_the_author_meta("user_url")!=""):?><a class="author" href="<?php the_author_meta("user_url"); ?>" title="<?php the_author(); ?>"><?php the_author(); ?></a><?php else: the_author(); endif; ?></li>
								<?php
								$categories = get_the_category();
								foreach($categories as $key=>$category)
								{
									?>
									<li>
										<a href="<?php echo get_category_link($category->term_id ); ?>" title="<?php echo (empty($category->description) ? sprintf(__('View all posts filed under %s', 'gymbase'), $category->name) : esc_attr(strip_tags(apply_filters('category_description', $category->description, $category)))); ?>">
											<?php echo $category->name; ?>
										</a>
									</li>
								<?php
								}
								?>
							</ul>
						</div>-->
							</div>
						</div>
					</div>
<?php
	global $wpdb;
	$wpdb->db_connect();
	$query1 = "SELECT * FROM `wp_term_relationships` as a inner join `wp_term_taxonomy` as b on a.`term_taxonomy_id` = b.`term_taxonomy_id` WHERE a.`object_id` = ".get_the_ID();
	$myrows = $wpdb->get_row($query1);	$category = $myrows->term_id;
	$query2 = "SELECT * FROM `wp_terms` WHERE `term_id` = ".$category;
	$myrows = $wpdb->get_row($query2);
?>
					<div class="vc_row wpb_row vc_row-fluid">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">			
								<div class="wpb_text_column wpb_content_element  vc_custom_1442469554697 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<h6 style="text-align: center;"><?php echo 'Profile last updated  '. $post -> post_modified; ?></h6>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">			
								<div class="wpb_text_column wpb_content_element  vc_custom_1442469554697 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<h3 style="text-align: center;"><strong><?php the_title(); ?></strong></h3>
									</div>
								</div>
								<div class="wpb_text_column wpb_content_element  vc_custom_1442453709804 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<h4 style="text-align: center;"><?php echo $myrows->slug; ?></h4>
									</div>
								</div>
								<div class="wpb_text_column wpb_content_element  vc_custom_1442453709804 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<h4 style="text-align: center;">
<?php 
	$zh_list = get_post_meta( get_the_ID(), 'zh_list', true );
	echo $zh_list;
	if( $zh_list > 0 ) {
		for ($i=0; $i < $zh_list; $i++) { 
			echo $title = get_post_meta( $post->ID, 'zh_list_' . $i . '_title', true );
			echo $img = get_post_meta( $post->ID, 'zh_list_' . $i . '_img', true );
			echo $desc = get_post_meta( $post->ID, 'zh_list_' . $i . '_desc', true );
		}
	}
?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">			
								<div class="wpb_text_column wpb_content_element  vc_custom_1442469554697 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<h6 style="text-align: center;"><?= esc_attr(get_post_meta($post->ID, "Key_Title_1", true));?> at <?= esc_attr(get_post_meta($post->ID, "Organisation1", true));?></h6>
										<h6 style="text-align: center;"><?= esc_attr(get_post_meta($post->ID, "Key_Title_2", true));?> at <?= esc_attr(get_post_meta($post->ID, "Organisation2", true));?></h6>
										<h6 style="text-align: center;"><?= esc_attr(get_post_meta($post->ID, "Key_Title_3", true));?> at <?= esc_attr(get_post_meta($post->ID, "Organisation3", true));?></h6>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid ">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">								
								<div class="wpb_text_column wpb_content_element vc_custom_1442469232786">
									<div class="wpb_wrapper">
										
										<p style="text-align: center;"><strong><span style="color: #ffffff;">
										Biography
										</span></strong></p>
									</div> 
								</div> 
								<div class="wpb_text_column wpb_content_element  vc_custom_1442470500400 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<p style="text-align: justify;"><?= esc_attr(get_post_meta($post->ID, "Biography", true));?></p>
									</div> 
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid ">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">
								<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
							</div> 
						</div> 
					</div>
					<div class="vc_row wpb_row vc_row-fluid ">
						<div class="vc_col-sm-12 wpb_column vc_column_container ">
							<div class="wpb_wrapper">								
								<div class="wpb_text_column wpb_content_element vc_custom_1442469232786">
									<div class="wpb_wrapper">
										<p style="text-align: center;"><span style="color: #ffffff;"><b><i>Interests</i></b></span></p>

									</div> 
								</div> 
								<!--<div class="wpb_text_column wpb_content_element vc_custom_1442457885690 wpb_animate_when_almost_visible wpb_bottom-to-top">
									<div class="wpb_wrapper">
										<p style="text-align: justify;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
									</div> 
								</div>-->
								<div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
									<span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
									<span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
								</div>
								<div class="vc_row wpb_row vc_row-fluid vc_custom_1442469823735">
									<div class="vc_col-sm-4 wpb_column vc_column_container">
										<div class="wpb_wrapper">											
											<div class="wpb_text_column wpb_content_element vc_custom_1442458537808 wpb_animate_when_almost_visible wpb_left-to-right">
												<div class="wpb_wrapper">
													<p style="text-align: center;"><span style="color: #ffffff;"><i>Clinical</i></span></p>
												</div> 
											</div>
											<div class="wpb_text_column wpb_content_element  vc_custom_1442479230795 wpb_animate_when_almost_visible wpb_bottom-to-top">
												<div class="wpb_wrapper">
													<ul>
														<?php
														$totalclinical= esc_attr(get_post_meta($post->ID, "totalclinical", true));
														if($totalclinical==0){
															$totalclinical=1;
														}
														for($x=0;$x<$totalclinical;$x++){
														?>
														<li><?= esc_attr(get_post_meta($post->ID, "clinical".$x."Disease", true)); ?> (<?= esc_attr(get_post_meta($post->ID, "clinical".$x."cases", true)); ?>)</li>
														<?php } ?>
													</ul>
												</div> 
											</div>  
										</div> 
									</div> 
									<div class="vc_col-sm-4 wpb_column vc_column_container">
										<div class="wpb_wrapper">									
											<div class="wpb_text_column wpb_content_element  vc_custom_1442458537808 wpb_animate_when_almost_visible wpb_left-to-right">
												<div class="wpb_wrapper">
													<p style="text-align: center;"><span style="color: #ffffff;"><i>Surgical</i></span></p>
												</div> 
											</div> 
											<div class="wpb_text_column wpb_content_element  vc_custom_1442479230795 wpb_animate_when_almost_visible wpb_bottom-to-top">
												<div class="wpb_wrapper">
													<ul>
														<?php
														$totalclinical= esc_attr(get_post_meta($post->ID, "totalsurgical", true));
														if($totalclinical==0){
															$totalclinical=1;
														}
														for($x=0;$x<$totalclinical;$x++){
														?>
														<li><?= esc_attr(get_post_meta($post->ID, "surgical".$x."Disease", true)); ?> (<?= esc_attr(get_post_meta($post->ID, "surgical".$x."cases", true)); ?>)</li>
														<?php } ?>
													</ul>
												</div> 
											</div> 
										</div> 
									</div> 
									<div class="vc_col-sm-4 wpb_column vc_column_container">
										<div class="wpb_wrapper">
											<div class="wpb_text_column wpb_content_element  vc_custom_1442458537808 wpb_animate_when_almost_visible wpb_left-to-right">
												<div class="wpb_wrapper">
													<p style="text-align: center;"><span style="color: #ffffff;"><i>Research</i></span></p>
												</div> 
											</div> 
											<div class="wpb_text_column wpb_content_element  vc_custom_1442479230795 wpb_animate_when_almost_visible wpb_bottom-to-top">
												<div class="wpb_wrapper">
													<ul>
														<?php
														$totalclinical= esc_attr(get_post_meta($post->ID, "totalresearch", true));
														if($totalclinical==0){
															$totalclinical=1;
														}
														for($x=0;$x<$totalclinical;$x++){
														?>
														<li><?= esc_attr(get_post_meta($post->ID, "research".$x."Disease", true)); ?> (<?= esc_attr(get_post_meta($post->ID, "research".$x."cases", true)); ?>)</li>
														<?php } ?>
													</ul>
												</div> 
											</div> 
										</div> 
									</div>
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">													
											<div class="wpb_text_column wpb_content_element vc_custom_1442469232786">
												<div class="wpb_wrapper">
													<p style="text-align: center;"><span style="color: #ffffff;"><b><i>Spoken Language</i></b></span></p>
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid vc_custom_1442469823735">
									<?php $langs=esc_attr(get_post_meta($post->ID, "Spoken_Languages", true));
									$langs=explode("/",$langs);
									foreach($langs as $lang){
									?>
									<!--vc_col-sm-4 wpb_column vc_column_container-->
									<div class="col-md-4">
										<div class="wpb_wrapper">											
											<div class="wpb_text_column wpb_content_element  vc_custom_1442467344544 wpb_animate_when_almost_visible wpb_bottom-to-top">
												<div class="wpb_wrapper">
													<p style="text-align: center;"><span style="color: #ffffff;"><?= $lang; ?></span></p>
												</div> 
											</div> 
										</div> 
									</div> 
									<?php } ?>
									<div class="vc_col-sm-3 wpb_column vc_column_container ">
										<div class="wpb_wrapper">											
										</div> 
									</div> 
									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">											
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
									<div class="wpb_text_column wpb_content_element vc_custom_1442469232786">
										<div class="wpb_wrapper">
											<p style="text-align: center;"><span style="color: #ffffff;"><b><i>M.D. License</i></b></span></p>

										</div> 
									</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<?php $international_License=esc_attr(get_post_meta($post->ID, "international_License", true));
									$international_License=explode("/",$international_License);
									foreach($international_License as $int){
									$key = array_search($int, $country_codes);
									?>
									<!--vc_col-sm-4 wpb_column vc_column_container -->
									<div class="col-md-2">
										<div class="wpb_wrapper">
											<div class="wpb_single_image wpb_content_element vc_custom_1442467110419 vc_align_center flag-cont  cntry-name">
												<div class="wpb_wrapper">
													<div class="img-thumbnail flag flag-icon-background flag-icon-<?= $key; ?>"></div>
												</div> 
											</div> 
											<div class="wpb_text_column wpb_content_element cntry-name">
												<div class="wpb_wrapper">
													<h6 style=""><span style="color: #808080;"><?= $int; ?></span></h6>

												</div> 
											</div> 
										</div> 
									</div> 
									<?php } ?>
									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
										</div> 
									</div> 

									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">											
											
										</div> 
									</div> 

									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
										</div> 
									</div> 

									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
										</div> 
									</div> 

									<div class="vc_col-sm-2 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>


										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
									<div class="wpb_text_column wpb_content_element vc_custom_1442469232786">
										<div class="wpb_wrapper">
											<p style="text-align: center;"><span style="color: #ffffff;"><b><i>Updates</i></b></span></p>

										</div> 
									</div> 
										</div> 
									</div> 
								</div>
								<?php
								$args = array(
								   'meta_query' => array(
									   array(
										   'key' => 'medicenter_doctor_author',
										   'value' => $post->ID,
										 )
								   )
								);
								$query = new WP_Query($args);
								
								
								if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
								$query->the_post();
								?>
								<div class="vc_row wpb_row vc_row-fluid vc_custom_1442469733599">
									<div class="vc_col-sm-3 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
									<div class="wpb_single_image wpb_content_element vc_custom_1442479340061 vc_align_center">
										<div class="wpb_wrapper">
											
											<img width="300" height="205" src="<?= wp_get_attachment_url( get_post_thumbnail_id($query->post->ID) );?>" class=" vc_box_border_grey attachment-medium" alt="<?php the_title(); ?>" />
										</div> 
									</div> 
										</div> 
									</div> 

									<div class="vc_col-sm-9 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
									<div class="wpb_text_column wpb_content_element  vc_custom_1442468607104">
										<div class="wpb_wrapper">
											<h4 style="text-align: justify;"><?php the_title(); ?></h4>

										</div> 
									</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											<div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_grey">
												<span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
												<span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
											</div>
										</div> 
									</div> 
								</div>
								<?php }}else{
									echo '<p style="text-align: justify;">No Updates</p>';
								}?>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											
									<div class="wpb_text_column wpb_content_element  vc_custom_1442470918614">
										<div class="wpb_wrapper">
											<p style="text-align: center;"><span style="color: #ffffff;"><b><i>Assistant</i></b></span></p>

										</div> 
									</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid vc_custom_1442470878105">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">											
											<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_bottom-to-top vc_align_center">
												<div class="wpb_wrapper">													
													<img width="150" height="150" src=<?php echo get_home_url()."/wp-content/uploads/2015/09/Icon_10_Call_center_agent-01.png"?> class=" vc_box_border_grey attachment-thumbnail" alt="Icon_10_Call_center_agent-01" />
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid ">
									<div class="vc_col-sm-12 wpb_column vc_column_container ">
										<div class="wpb_wrapper">
											<div class="vc_empty_space"  style="height: 26px" ><span class="vc_empty_space_inner"></span></div>
										</div> 
									</div> 
								</div>
							</div> 
						</div> 
					</div>
				<?php
				endwhile; endif;
				?>
			</div>
			<?php
//			comments_template();
//			require_once("comments-form.php");
//			require_once("custom_colors.php");
			?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/flags/assets/docs.js"></script>
<?php
get_footer();
/*
?>
<?php

//Template Name: Single post

get_header();
setPostViews(get_the_ID());
?>
<div class="theme_page relative">
	<div class="page_layout page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<h1 class="page_title"><?php the_title(); ?></h1>
				<ul class="bread_crumb">
					<li>
						<a href="<?php echo get_home_url(); ?>" title="<?php _e('Home', 'medicenter'); ?>">
							<?php _e('Home', 'medicenter'); ?>
						</a>
					</li>
					<li class="separator icon_small_arrow right_gray">
						&nbsp;
					</li>
					<li>
						<?php the_title(); ?>
					</li>
				</ul>
			</div>
			<?php
//get page with single post template set
			$post_template_page_array = get_pages(array(
				'post_type' => 'post',
				'post_status' => 'publish',
				//'number' => 1,
				//'meta_key' => '_wp_page_template',
				'meta_value' => 'single.php'
			));
			$post_template_page = $post_template_page_array[0];
			$sidebar = get_post(get_post_meta($post_template_page->ID, "page_sidebar_header", true));
			if(!(int)get_post_meta($sidebar->ID, "hidden", true) && is_active_sidebar($sidebar->post_name)):
			?>
			<div class="page_header_right">
				<?php
				dynamic_sidebar($sidebar->post_name);
				?>
			</div>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix">
			<?php
			if(count($post_template_page_array) && isset($post_template_page))
			{
				echo wpb_js_remove_wpautop(apply_filters('the_content', $post_template_page->post_content));
				global $post;
				$post = $post_template_page;
				setup_postdata($post);
			}
			else
				echo wpb_js_remove_wpautop(apply_filters('the_content', '[vc_row top_margin="none" el_position="first last"] [vc_column top_margin="none" width="2/3"] [single_post featured_image_size="default" columns="1" show_post_categories="1" show_post_author="1" comments="1" lightbox_icon_color="blue_light" el_position="first last"] [/vc_column] [vc_column top_margin="none" width="1/3"] [vc_widget_sidebar sidebar_id="sidebar-blog" top_margin="page_margin_top" el_position="first"] [box_header title="Photostream" type="h3" bottom_border="1" top_margin="page_margin_top_section"] [photostream id="photostream" images="21,15,16,17,18,19" featured_image_size="default" lightbox_icon_color="blue_light" images_loop="1" top_margin="none"] [vc_widget_sidebar sidebar_id="sidebar-blog-2" top_margin="page_margin_top_section" el_position="last"] [/vc_column] [/vc_row]'));
			?>
		</div>
	</div>
</div>
<?php
get_footer();
*/
?>
