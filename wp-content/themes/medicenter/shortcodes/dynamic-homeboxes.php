<?php
//dynamic homeboxes
function dynamic_homeboxes($atts, $content)
{
	global $themename;
	global $post;
	extract(shortcode_atts(array(
		"box_bacground_color_1" => "#42b3e5",
		"box_bacground_color_2" => "#43a140",
		"box_bacground_color_3" => "#f17800",
		"box_bacground_color_4" => "#ffa800",
		"box_bacground_color_5" => "#ab5591",
		"box_bacground_color_6" => "#0097b5",
	), $atts));
	 
	$box_count = 6;
	$day_offset = 0;
	$output = "";
	
	$output .= "<ul class='home_box_container_list dynamic-homeboxes clearfix'>";
	
	while($box_count) {
		$weekday = new DateTime(current_time("Y-m-d H:i:s"));		
		$weekday->modify("+{$day_offset} day");
		if($weekday->format("N")!=7) {
			$box_background_color = 'box_bacground_color_' . (7-$box_count);
			$box_title = ($box_count==6 && $day_offset==0 ? 
				__("Today's Story", "medicenter") : 
				($box_count==5 && $day_offset==1 ? 
					__("Comming Tomorrow", "medicenter") :
					date_i18n("l", $weekday->getTimestamp()) . " (" . $weekday->format("Y-m-d") . ")"));
			query_posts(array(
				"post_type"			=>	"post",
				"post_status"			=>	array("draft", "publish", "future"),
				"day"				=>	$weekday->format("d"),
				"monthnum"			=>	$weekday->format("m"),
				"year"				=>	$weekday->format("Y"),
				"orderby"			=>	"date",
				"order"				=>	"asc",
				"posts_per_page"	=>	1,
			));
			// the Loop
			
			if(have_posts()) {
				the_post();
				// publish only todays post
				if($day_offset==0) {
					$post->post_status = "publish";
					wp_update_post();
				}
				//get doctor information
				$doctor_author = get_post_meta(get_the_ID(), $themename. "_doctor_author", true);
				if($doctor_author>0) {
					$doctors = get_posts(array(
						"p"					=>	$doctor_author,
						"post_type"			=>	"doctors",
						"post_status"		=>	"publish",
						"posts_per_page"	=>	1,
					));
					$doctor_categories = wp_get_post_terms($doctors[0]->ID, "doctors_category");
				}
				$doctor = array(
					"name"			=>	!empty($doctors[0]->post_title) ? $doctors[0]->post_title : "",
					"subtitle"		=>	!empty($doctors[0]->ID) ? get_post_meta($doctors[0]->ID, "subtitle", true) : "",
					"speciality"	=>	!empty($doctor_categories[0]->name) ? $doctor_categories[0]->name : "",
					"thumbnail"		=>	!empty($doctors[0]->ID) ? get_the_post_thumbnail($doctors[0]->ID, $themename . "-small-thumb", array('class'	=> "avatar")) : "",
					"profil_page"	=>	!empty($doctors[0]->ID) ? get_post_permalink($doctors[0]->ID) : "",
				);
				

				$post_title = strlen(get_the_title())>165 ? mb_substr(get_the_title(), 0, 55) . "..." : get_the_title();
				$content = ($day_offset==0 ? 
							"<a class='post-title' href='" . get_permalink() . "' title='{$post_title}'>{$post_title}</a>" : 
							"<div class='text'>{$post_title}</div>") .
						"<div class='doctor-info'>" . 
							(!empty($doctor['name']) ? 
								"<a href='" . $doctor['profil_page'] . "' target='_blank'>" . 
								$doctor['thumbnail'] . 
								"<p>" . $doctor['name'] . "<br/>" .
								(!empty($doctor['subtitle']) ? $doctor['subtitle'] . "<br/>" : "") .
								$doctor['speciality'] . "</p>" . 
								"</a>"
								:
								"<p>No doctor</p>");
				$read_more_link = ($day_offset==0 && false ? 
							"<a title='" . __("Read more", "medicenter") . "' href='" . get_permalink(). "' target='_blank' class='read-more'>" . __("Read more", "medicenter") . "</a>" : 
								"");
			} else {
				$content = "<div class='text'>" . __("No post available.", "medicenter") . "</div>";
				$read_more_link = "";
			}
			
			$output .= "<li class='home_box_container clearfix' style='background-color: {$$box_background_color};'>
				<div class='home_box'>
				<div class='var-dump' style='display: none;'>{$weekday->format("Y-m-d H:i:s")}</div>
					<h2>{$box_title}</h2>
					<div class='news clearfix'>
						{$content}
					</div>
					{$read_more_link}
				</li>";				
			$box_count--;
		}
		$day_offset++;
	}
	
	wp_reset_query();
	$output .= "</ul>";
	return $output;
}
add_shortcode("dynamic_homeboxes", "dynamic_homeboxes");

//visual composer
wpb_map( array(
	"name" => __("Post homeboxes", 'medicenter'),
	"base" => "dynamic_homeboxes",
	"class" => "",
	"controls" => "full",
	"show_settings_on_create" => true,
	"category" => __('MediCenter', 'medicenter'),
	"params" => array(
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 1", 'medicenter'),
			"param_name" => "box_bacground_color_1",
			"value" => "#42b3e5"
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 2", 'medicenter'),
			"param_name" => "box_bacground_color_2",
			"value" => "#43a140"
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 3", 'medicenter'),
			"param_name" => "box_bacground_color_3",
			"value" => "#f17800"
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 4", 'medicenter'),
			"param_name" => "box_bacground_color_4",
			"value" => "#ffa800"
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 5", 'medicenter'),
			"param_name" => "box_bacground_color_5",
			"value" => "#ab5591"
		),
		
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Set background color for HomeBox 6", 'medicenter'),
			"param_name" => "box_bacground_color_6",
			"value" => "#0097b5"
		),
	)
));